﻿using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Developers.OnlineStore.Data.BLL.Interfaces
{
    public interface IRole
    {
        /// <summary>
        /// Получение роли по ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Role</returns>
        Task<RoleDTO> GetRole(Guid id);


        /// <summary>
        /// Получение набора ролей  по idEmpl
        /// </summary>
        /// <param name="idEmpl">Код пользователя</param>
        /// <returns>Role</returns>
        Task<RoleDTO> GetRoleEmpl(Guid idEmpl);

        /// <summary>
        /// Добавить роль
        /// </summary>
        /// <param name="row">новая роль</param>
        /// <param name="userAccId">ID авторизированного пользователя</param>
        /// <returns>newID</returns>
        Task AddRole(RoleDTO row);

        /// <summary>
        /// Изменить роль
        /// </summary>
        /// <param name="row">запись для изменения</param>
        /// <param name="userAccId">ID авторизированного пользователя</param>
        /// <returns>updID</returns>
        Task UpdRole(RoleDTO row);

        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <param name="delID">запись для удаления</param>
        /// <param name="del">true- удалить физически; false-поставить признак удаления</param>
        /// <returns>updID</returns>
        Task DelRole(Guid delID);

        /// <summary>
        /// Список ролей
        /// </summary>
        /// <returns>лист с данными </returns>
        Task<List<RoleDTO>> GetAllRoles();
    }
}
