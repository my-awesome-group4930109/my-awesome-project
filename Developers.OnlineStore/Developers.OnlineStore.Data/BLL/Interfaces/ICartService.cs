﻿using Developers.OnlineStore.Data.Contracts;
using System;
using System.Threading.Tasks;

namespace Developers.OnlineStore.Data.BLL.Interfaces
{
    public interface ICartService
    {
        /// <summary>
        /// Получить корзину
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Корзина с товарами</returns>
        Task <CartDTO> GetCart (Guid id);
        /// <summary>
        /// Создать корзину с товарами
        /// </summary>
        /// <param name="cartDTO"></param>
        /// <returns>ID корзины</returns>
        Task<Guid> AddCart (CartDTO cartDTO);
        /// <summary>
        /// Удалить существующую корзину
        /// </summary>
        /// <param name="id"></param>
        Task DeleteCart (Guid id);
        /// <summary>
        /// Изменить состав корзины
        /// </summary>
        /// <param name="cartDTO"></param>
        /// <returns>Новый состав корзины</returns>
        Task<CartDTO> UpdateCart(CartDTO cartDTO);

    }
}
