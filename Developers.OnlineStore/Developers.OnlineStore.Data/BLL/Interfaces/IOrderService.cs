﻿using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Data.Contracts;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System;

namespace Developers.OnlineStore.Data.BLL.Interfaces
{
    public interface IOrderService
    {
        /// <summary>
        /// Получить заказ по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>OrderDTO</returns>
        Task<OrderDTO> GetOrderById(Guid id);

        /// <summary>
        /// Получение всех заказов покупателя по ID
        /// </summary>
        /// <param name="id">ID покупателя</param>
        /// <returns>OrderDTO</returns>
        Task<IEnumerable<OrderDTO>> GetOrdersByCustomerID(Guid id);

        /// <summary>
        /// Получить список заказов по фильтру
        /// </summary>
        /// <param name="filter"></param>
        /// <returns>OrderDTO</returns>
        Task<IEnumerable<OrderDTO>> GetOrdersByFilter(Expression<Func<Order, bool>> filter);

        /// <summary>
        /// Получить все заказы
        /// </summary>
        /// <returns>OrderDTO</returns>
        Task<IEnumerable<OrderDTO>> GetAllOrders();

        /// <summary>
        /// Создать заказ
        /// </summary>
        /// <param name="orderDTO"></param>
        /// <returns>Guid</returns>
        Task<Guid> AddOrderAsync(OrderDTO orderDTO);

        /// <summary>
        /// Удалить заказ
        /// </summary>
        /// <param name="id"></param>        
        Task DeleteOrderAsync(Guid id);

        /// <summary>
        /// Обновить информацию о заказе
        /// </summary>
        /// <param name="employeeDTO"></param>        
        Task<OrderDTO> UpdateOrderAsync(OrderDTO orderDTO);
    }
}
