﻿using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Developers.OnlineStore.Data.BLL.Interfaces
{
    public interface IEmployeeService
    {
        /// <summary>
        /// Получить сотрудника по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>EmployeeDTO</returns>
        Task<EmployeeDTO> GetById(Guid id);

        /// <summary>
        /// Получить список сотрудников по фильтру
        /// </summary>
        /// <param name="filter"></param>
        /// <returns>Employees</returns>
        Task<IEnumerable<EmployeeDTO>> GetEmployees(Expression<Func<Employee, bool>> filter);
        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <param name="employeeDTO"></param>
        /// <returns>Guid</returns>
        Task<Guid> AddEmployeeAsync(EmployeeDTO employeeDTO);
        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id"></param>        
        Task DeleteEmployeeAsync(Guid id);
        /// <summary>
        /// Обновить информацию о сотруднике
        /// </summary>
        /// <param name="employeeDTO"></param>        
        Task UpdateEmployeeAsync(EmployeeDTO employeeDTO);


    }
}
