﻿using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Developers.OnlineStore.Data.BLL.Interfaces
{
    public interface IProduct
    {
        Task<ProductDTO> GetProduct(Guid id);

        Task<List<ProductDTO>> GetListProducts(Expression<Func<Product, bool>> filter = null);

        Task<ProductDTO> AddProduct(ProductDTO product);
    }
}
