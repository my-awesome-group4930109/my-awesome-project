﻿using AutoMapper;
using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Core.Repositories;
using Developers.OnlineStore.Data.BLL.Interfaces;
using Developers.OnlineStore.Core.DAL.Infrastructure;
using Developers.OnlineStore.Data.Contracts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Collections;
using System.Security.Cryptography;

namespace Developers.OnlineStore.Data.BLL.Services
{
    public class EmployeeService:BaseService,IEmployeeService
    {
        public EmployeeService(IUnitOfWork uow, ILogger<EmployeeService> logger, IMapper mapper) : base(uow, logger, mapper) 
        { 
        }
       
        public async Task<Guid> AddEmployeeAsync(EmployeeDTO employeeDTO)
        {
            var entity = _mapper.Map<Employee>(employeeDTO);
            entity.Id=Guid.NewGuid().ToString();         
            await _unitOfWork.GetRepository<Employee>().AddAsync(entity);
            return Guid.Parse(entity.Id);
        }

        public async Task DeleteEmployeeAsync(Guid id)
        {
            await _unitOfWork.GetRepository<Employee>().RemoveAsync(id);
        }

        public async Task<EmployeeDTO> GetById(Guid id)
        {
            var entity = await _unitOfWork.GetRepository<Employee>().GetByIdAsync(id);
            if(entity == null)
            {
                throw new ValidationException("пользователь не найден", "");
            }
            var employee = _mapper.Map<EmployeeDTO>(entity);
            return employee;
        }

        public async Task<IEnumerable<EmployeeDTO>> GetEmployees(Expression<Func<Employee, bool>> filter)
        {
            var entities = await _unitOfWork.GetRepository<Employee>().GetByFilterAsync(filter);
            if (entities ==null)
                throw new ValidationException("Пользователи не найдены!", "");
            IEnumerable<EmployeeDTO> employees = _mapper.Map<IEnumerable<EmployeeDTO>>(entities);
            return employees;
        }

        public async Task UpdateEmployeeAsync(EmployeeDTO employeeDTO)
        {
            var entity = _mapper.Map<Employee>(employeeDTO);
            await _unitOfWork.GetRepository<Employee>().UpdateAsync(entity);
        }
    }
}
