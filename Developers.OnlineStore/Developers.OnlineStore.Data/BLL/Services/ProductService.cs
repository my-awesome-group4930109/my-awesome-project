﻿using AutoMapper;
using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Core.Repositories;
using Developers.OnlineStore.Data.BLL.Interfaces;
using Developers.OnlineStore.Data.Contracts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Developers.OnlineStore.Data.BLL.Services
{
    public class ProductService :BaseService,IProduct
    {

        public ProductService(IUnitOfWork uow, ILogger<RoleService> logger, IMapper mapper) : base(uow, logger, mapper)
        {
        }

        public async Task<List<ProductDTO>> GetListProducts(Expression<Func<Product, bool>> filter = null)
        {
            var list_ = await _unitOfWork.GetRepository<Product>().GetByFilterAsync(filter);
            if (!list_.Any())
                throw new ValidationException("Продукция не найдена");
            List<ProductDTO> map_products = _mapper.Map<List<ProductDTO>>(list_);
            return map_products;
        }

        public async Task<ProductDTO> GetProduct(Guid id)
        {
            var product = await _unitOfWork.GetRepository<Product>().GetByIdAsync(id);
            if (product == null)
                throw new ValidationException("Продукция не найдена");
            return _mapper.Map<ProductDTO>(product);
        }
        public async Task<ProductDTO> AddProduct(ProductDTO product)
        {
            product.Id = Guid.NewGuid().ToString();
             await _unitOfWork.GetRepository<Product>().AddAsync(_mapper.Map<Product>(product));
            return product;
        }
    }
}
