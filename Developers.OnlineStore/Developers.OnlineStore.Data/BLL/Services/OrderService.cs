﻿using AutoMapper;
using Developers.OnlineStore.Core.DAL.Infrastructure;
using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Core.Repositories;
using Developers.OnlineStore.Data.BLL.Interfaces;
using Developers.OnlineStore.Data.Contracts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Developers.OnlineStore.Data.BLL.Services
{
    public class OrderService : BaseService, IOrderService
    {
        public OrderService(IUnitOfWork uow, ILogger<OrderService> logger, IMapper mapper) : base(uow, logger, mapper)
        {
        }
        public async Task<Guid> AddOrderAsync(OrderDTO orderDTO)
        {
            orderDTO.Id = Guid.NewGuid().ToString();
            foreach(var s in orderDTO.OrderStatuses)
            {
                s.Id = Guid.NewGuid().ToString();
                s.OrderId = orderDTO.Id;
                s.CreationDate = DateTime.Now.ToString();
                s.DeliveryDate= DateTime.Now.AddDays(3.0).ToString();                
            }           
            foreach(var p in orderDTO.ProductOrders)
            {
                p.Id = Guid.NewGuid().ToString();
                p.OrderId = orderDTO.Id;                    
            }
          
            var entity = _mapper.Map<Order>(orderDTO);
            await _unitOfWork.GetRepository<Order>().AddAsync(entity);
            return Guid.Parse(orderDTO.Id);
        }

        public async Task DeleteOrderAsync(Guid id)
        {
            await _unitOfWork.GetRepository<Order>().RemoveAsync(id);
        }

        public async Task<IEnumerable<OrderDTO>> GetAllOrders()
        {
            var entities = await _unitOfWork.GetRepository<Order>().GetAllAsync(x=>x.OrderStatuses);
            var orders = _mapper.Map<IEnumerable<OrderDTO>>(entities);
            return orders;
        }

        public async Task<IEnumerable<OrderDTO>> GetOrdersByCustomerID(Guid id)
        {
            var entities = await _unitOfWork.GetRepository<Order>().GetByFilterAsync(x => x.CustomerId == id.ToString(), x=>x.OrderStatuses, x=> x.StoreAdress,x=>x.ProductOrders) ;
            var orders = _mapper.Map<IEnumerable<OrderDTO>>(entities);
            return orders;
        }

        public async Task<OrderDTO> GetOrderById(Guid id)
        {
            var entity = await _unitOfWork.GetRepository<Order>().GetByIdAsync(id, x => x.OrderStatuses, x => x.StoreAdress, x => x.ProductOrders);
            var order = _mapper.Map<OrderDTO>(entity);
            return order;
        }

        public async Task<IEnumerable<OrderDTO>> GetOrdersByFilter(Expression<Func<Order, bool>> filter)
        {
            var entities = await _unitOfWork.GetRepository<Order>().GetByFilterAsync(filter);
            var orders = _mapper.Map<IEnumerable<OrderDTO>>(entities);
            return orders;
        }

        public async Task<OrderDTO> UpdateOrderAsync(OrderDTO orderDTO)
        {
            var entity = _mapper.Map<Order>(orderDTO);
            try
            {
                await _unitOfWork.GetRepository<Order>().UpdateAsync(entity);
            }
            catch
            {
                throw new ValidationException("Не удалось обновить заказ", "");
            }
            var newEntity = await _unitOfWork.GetRepository<Order>().GetByIdAsync(Guid.Parse(entity.Id));
            var result = _mapper.Map<OrderDTO>(newEntity);
            return result;
        }
    }
}
