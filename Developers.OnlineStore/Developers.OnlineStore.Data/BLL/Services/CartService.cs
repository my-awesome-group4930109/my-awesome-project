﻿using AutoMapper;
using Developers.OnlineStore.Core.DAL.Infrastructure;
using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Core.Repositories;
using Developers.OnlineStore.Data.BLL.Interfaces;
using Developers.OnlineStore.Data.Contracts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Developers.OnlineStore.Data.BLL.Services
{
    public class CartService : BaseService, ICartService
    {
        public CartService(IUnitOfWork uow, ILogger<CartService> logger, IMapper mapper) : base(uow,logger, mapper)
        {
        }
        public async Task<Guid> AddCart(CartDTO cartDTO)
        {
            
            var entity = _mapper.Map<Cart>(cartDTO);
            entity.Id= Guid.NewGuid().ToString();
            foreach (var p in entity.ProductCarts)
            {
                p.CartId = entity.Id;
                p.Id = Guid.NewGuid().ToString();  
            }


            await _unitOfWork.GetRepository<Cart>().AddAsync(entity);
            return Guid.Parse(entity.Id);
        }
          
public async Task DeleteCart(Guid id)
        {
            await _unitOfWork.GetRepository<Cart>().RemoveAsync(id);
        }

        public async Task<CartDTO> GetCart(Guid id)
        {
            var entity = await _unitOfWork.GetRepository<Cart>().GetByIdAsync(id,x=>x.ProductCarts);            
            if (entity == null)
            {
                throw new ValidationException("Не удалось загрузить корзину", "");
            }
            var result = _mapper.Map<CartDTO>(entity);
            return result;
        }

        public async Task<CartDTO> UpdateCart(CartDTO cartDTO)
        {
            var oldEntity = await _unitOfWork.GetRepository<Cart>().GetByIdAsync(Guid.Parse(cartDTO.Id), x => x.ProductCarts);
            var entity = _mapper.Map<Cart>(cartDTO);
            foreach (var p in entity.ProductCarts)
            {
                p.CartId = entity.Id;
                p.Id = oldEntity.ProductCarts.Select(x => x.Id).FirstOrDefault().ToString();
            }
            try
            {
                await _unitOfWork.GetRepository<Cart>().UpdateAsync(entity);
            }
            catch(Exception ex)
            {
                throw new ValidationException(ex.Message,"");
            }
           
           
            var newEntity = await _unitOfWork.GetRepository<Cart>().GetByIdAsync(Guid.Parse(entity.Id), x => x.ProductCarts);
            var result = _mapper.Map<CartDTO>(newEntity);
            return result;
        }
    }
}
