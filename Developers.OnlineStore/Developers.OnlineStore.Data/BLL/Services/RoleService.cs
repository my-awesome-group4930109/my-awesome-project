﻿using AutoMapper;
using Developers.OnlineStore.Core.DAL.Infrastructure;
using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Core.Repositories;
using Developers.OnlineStore.Data.BLL.Interfaces;
using Developers.OnlineStore.Data.Contracts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Developers.OnlineStore.Data.BLL.Services
{
    public class RoleService : BaseService, IRole
    {
        private IEmployeeService _user { get; set; }
        public RoleService(IEmployeeService user_, IUnitOfWork uow, ILogger<RoleService> logger, IMapper mapper) : base(uow, logger, mapper)
        {
            _user = user_;
        }
        /// <summary>
        /// Список ролей
        /// </summary>
        /// <returns>лист с данными </returns>
        public async Task<List<RoleDTO>> GetAllRoles()
        {
            return _mapper.Map<List<RoleDTO>>(await _unitOfWork.GetRepository<Role>().GetAllAsync());
        }

        /// <summary>
        /// Получение роли по ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Role</returns>
        public async Task<RoleDTO> GetRole(Guid id)
        {
            var role = await _unitOfWork.GetRepository<Role>().GetByIdAsync(id);
            return _mapper.Map<RoleDTO>(role);
        }
        /// <summary>
        /// Получение набора ролей  по idEmpl
        /// </summary>
        /// <param name="idEmpl">Код пользователя</param>
        /// <returns>Role</returns>
        public async Task<RoleDTO> GetRoleEmpl(Guid idEmpl)
        {
            try
            {
                List<RoleDTO> RoleListUser = new();
                var UserRole = (await _unitOfWork.GetRepository<Employee>().GetByFilterAsync(x => x.Id== idEmpl.ToString())).FirstOrDefault();
                if (UserRole == null)
                    throw new ValidationException("не найден пользователь", "");
                var list_ = await _unitOfWork.GetRepository<Role>().GetByFilterAsync(x => x.Id == UserRole.RoleId);
                return _mapper.Map<RoleDTO>(list_);
            }
            catch (Exception ex)
            {
                throw new ValidationException("Ошибка", ex.Message);
            }
        }
        /// <summary>
        /// Добавить роль
        /// </summary>
        /// <param name="row">новая роль</param>
        /// <param name="userAccId">ID авторизированного пользователя</param>
        /// <returns>newID</returns>
        public async Task AddRole(RoleDTO row)
        {
            if (await _unitOfWork.GetRepository<Role>().AnyAsync(Guid.Parse(row.Id)))
                throw new ValidationException($"Такая роль уже существует!", "");

            await _unitOfWork.GetRepository<Role>().AddAsync(_mapper.Map<Role>(row));
        }

        /// <summary>
        /// Изменить роль
        /// </summary>
        /// <param name="row">запись для изменения</param>
        /// <param name="userAccId">ID авторизированного пользователя</param>
        /// <returns>updID</returns>
        public async Task UpdRole(RoleDTO row)
        {
            var rowUpd_ = await _unitOfWork.GetRepository<Role>().GetByIdAsync(Guid.Parse(row.Id));
            if (rowUpd_ == null)
                throw new ValidationException($"Роль с id={row.Id} не найдена!", "");

            rowUpd_.Name = row.Name;
            await _unitOfWork.GetRepository<Role>().UpdateAsync(rowUpd_);
        }
        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <param name="delID">запись для удаления</param>
        /// <param name="del">true- удалить физически; false-поставить признак удаления</param>
        /// <returns>updID</returns>
        public async Task DelRole(Guid delID)
        {
            var rowDel_ = await _unitOfWork.GetRepository<Role>().GetByIdAsync(delID);
            if (rowDel_ == null)
                throw new ValidationException($"Роль с id={delID} не найдена!", "");
            await _unitOfWork.GetRepository<Role>().RemoveAsync(delID);
        }

    }
}
