﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Developers.OnlineStore.Core.Repositories;
using Microsoft.Extensions.Logging;

namespace Developers.OnlineStore.Data.BLL.Services
{
    public class BaseService
    {
        protected readonly IMapper _mapper;
        protected ILogger _logger;
        protected readonly IUnitOfWork _unitOfWork;
        public BaseService(IUnitOfWork uow, ILogger logger, IMapper mapper) : base()
        {
            _logger = logger;
            _mapper = mapper;
            _unitOfWork = uow;
        }
    }
}
