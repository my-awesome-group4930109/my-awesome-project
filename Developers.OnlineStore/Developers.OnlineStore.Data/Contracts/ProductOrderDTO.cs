﻿using Developers.OnlineStore.Core.Domain;

namespace Developers.OnlineStore.Data.Contracts
{
    public class ProductOrderDTO:BaseEntity
    {
        public string ProductId { get; set; }
        public string OrderId { get; set; }
        public double Price { get; set; }
    }
}
