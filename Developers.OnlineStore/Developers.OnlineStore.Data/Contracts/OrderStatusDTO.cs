﻿using Developers.OnlineStore.Core.Domain;

namespace Developers.OnlineStore.Data.Contracts
{
    public class OrderStatusDTO:BaseEntity
    {
        public string StatusId { get; set; }
        public string OrderId { get; set; }
        public string CreationDate { get; set; }
        public string DeliveryDate { get; set; }
        public virtual Order Order { get; set; }
        public virtual StatusState Status { get; set; }
    }
}
