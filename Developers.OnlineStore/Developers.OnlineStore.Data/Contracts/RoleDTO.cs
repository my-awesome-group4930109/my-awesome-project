﻿using Developers.OnlineStore.Core.Domain;
using System.Collections.Generic;

namespace Developers.OnlineStore.Data.Contracts
{
    public class RoleDTO:BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }

    }
}
