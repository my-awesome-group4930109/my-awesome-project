﻿using Developers.OnlineStore.Core.Domain;
using System;

namespace Developers.OnlineStore.Data.Contracts
{
    public class EmployeeDTO: BaseEntity
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string RoleId { get; set; }
    }
}
