﻿using System;

namespace Developers.OnlineStore.Core.Domain
{
    public class BaseEntity
    {
        public string Id { get; set; }
    }
}
