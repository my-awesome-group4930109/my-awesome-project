﻿using Developers.OnlineStore.Core.Domain;
using System.Collections.Generic;

namespace Developers.OnlineStore.Data.Contracts
{
    public class OrderDTO:BaseEntity
    {
        public string CustomerId { get; set; }
        public string StoreAdressId { get; set; }
        public string DateToDelivery { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual StoreAdress StoreAdress { get; set; }
        public virtual ICollection<OrderStatusDTO> OrderStatuses { get; set; }
        public virtual ICollection<ProductOrderDTO> ProductOrders { get; set; }
    }
}
