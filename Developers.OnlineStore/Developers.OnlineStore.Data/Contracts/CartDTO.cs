﻿using Developers.OnlineStore.Core.Domain;
using System.Collections.Generic;

namespace Developers.OnlineStore.Data.Contracts
{
    public class CartDTO:BaseEntity
    {
        public string CustomerId { get; set; }
        public virtual ICollection<ProductCartDTO> ProductCarts { get; set; }
    }
}
