﻿namespace Developers.OnlineStore.Data.Contracts
{
    public class ProductCartDTO
    {
        public string Id { get; set; }
        public string ProductId { get; set; }
        public string CartId { get; set; }
        public long Count { get; set; }
    }
}
