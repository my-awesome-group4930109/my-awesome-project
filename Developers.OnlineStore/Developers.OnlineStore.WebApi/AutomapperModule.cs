﻿using AutoMapper;
using Developers.OnlineStore.Data.Mapping;
using Developers.OnlineStore.WebApi.Automapper.Profiles;
using Developers.OnlineStore.WebApi.mapping;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Developers.OnlineStore.WebApi.Automapper
{
    public static class AutomapperModule
    {
        public static IServiceCollection AddAutomapperModule(this IServiceCollection services)
        {
            services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));
            return services;
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(config =>
            {
                config.AddProfile<ProductProfile>();
                config.AddProfile<EmployeeMappingProfile>();
                config.AddProfile<RoleProfile>();
                config.AddProfile<CartMappingProfile>();
                config.AddProfile<OrderMappingProfile>();
            });

            configuration.AssertConfigurationIsValid();

            return configuration;
        }

    }
}
