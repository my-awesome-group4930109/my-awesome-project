﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Developers.OnlineStore.WebApi.Middleware
{
    public class MetricsMiddleware
    {
        private readonly RequestDelegate _next;

        private const string MessageTemplate = "";

        public MetricsMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        public async Task InvokeAsync(HttpContext httpContext, ILogger<MetricsMiddleware> logger)
        {
            var timer = new Stopwatch();
            timer.Start();
            await _next(httpContext);
            timer.Stop();
            var elapsedMs = timer.Elapsed;


            logger.Log(LogLevel.Information, MessageTemplate, httpContext.Request.Method, httpContext.Request.Path,
                httpContext.Request.Query, httpContext.Request.QueryString, httpContext.Response?.StatusCode,
                elapsedMs);
        }
    }
    public static class MetricsMiddlewareAppExtensions
    {
        public static IApplicationBuilder UseMetricsMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<MetricsMiddleware>();
        }

    }
}
