﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Microsoft.EntityFrameworkCore;
using Developers.OnlineStore.Core.Repositories;
using Developers.OnlineStore.Data.Services;
using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Data.BLL.Interfaces;
using Developers.OnlineStore.Data.BLL.Services;

namespace Developers.OnlineStore.WebApi.Middleware
{ 
    public static class DataAccessModule
    {
        public sealed class ModuleConfiguration
        {
            public IServiceCollection Services { get; init; }
        }
        public static IServiceCollection AddDataAccessModule(this IServiceCollection services, Action<ModuleConfiguration> action)
        {
            var moduleConfiguration = new ModuleConfiguration
            {
                Services = services
            };
            action(moduleConfiguration);
            return services;
        }
        public static void IServiceCollection(this ModuleConfiguration moduleConfiguration, string connectionString)
        {
            moduleConfiguration.Services.AddDbContext<StoreContext>(options =>
            {
                options.UseSqlite(connectionString);
            });


            moduleConfiguration.Services.AddScoped<IUnitOfWork, UnitOfWork>();
            moduleConfiguration.Services.AddScoped<IProduct, ProductService>();
            moduleConfiguration.Services.AddScoped<IRole, RoleService>();
            moduleConfiguration.Services.AddTransient<IEmployeeService, EmployeeService>();
            moduleConfiguration.Services.AddTransient<ICartService, CartService>();
            moduleConfiguration.Services.AddTransient<IOrderService, OrderService>();

        }
    }
}
