﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Developers.OnlineStore.WebApi.Middleware
{
    public class GlobalExceptionHandler
    {
        private readonly RequestDelegate _next;
        public GlobalExceptionHandler(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context,ILogger<GlobalExceptionHandler> logger, IWebHostEnvironment webHostEnvironment)
        {
            context.Request.EnableBuffering();
            try
            {
                await _next(context);
            }
            catch(Exception ex)
            {
                await ExceptionHandler(context, ex, logger, webHostEnvironment);
            }
        }
        private static Task ExceptionHandler(HttpContext context, Exception ex, ILogger<GlobalExceptionHandler> logger,IWebHostEnvironment webHostEnvironment)
        {
            logger.LogError(ex, ex.Message);
            var errorMessageDetails = string.Empty;
            if (!webHostEnvironment.IsProduction())
                errorMessageDetails = $"Ошибка: {ex.Message} в блоке вызова: {ex.StackTrace}";
            else
                errorMessageDetails = $"Ошибка {ex.Message}. Пожалуйста, обратитесь к администратору.";
            var result = JsonConvert.SerializeObject(new { error = errorMessageDetails });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return context.Response.WriteAsync(result);
        }        
    }
    public static class HttpStatusCodeExceptionMiddlewareExtensions
    {
        public static IApplicationBuilder UseGlobalExceptionHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<GlobalExceptionHandler>();
        }

    }
}
