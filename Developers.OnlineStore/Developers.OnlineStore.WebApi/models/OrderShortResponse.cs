﻿namespace Developers.OnlineStore.WebApi.models
{
    public class OrderShortResponse:BaseSysModel
    {
        public string CustomerId { get; set; }
        public string StoreAdressId { get; set; }
        public string StatusId { get; set; }
    }
}
