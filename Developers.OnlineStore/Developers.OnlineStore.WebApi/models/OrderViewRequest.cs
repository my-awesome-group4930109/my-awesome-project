﻿using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Data.Contracts;
using System.Collections.Generic;

namespace Developers.OnlineStore.WebApi.models
{
    public class OrderViewRequest:BaseSysModel
    {
        public string CustomerId { get; set; }
        public string StoreAdressId { get; set; }
        public string DateToDelivery { get; set; }

        public virtual ICollection<OrderStatusView> OrderStatuses { get; set; }
        public virtual ICollection<ProductOrderViewRequest> ProductOrders { get; set; }
    }
}
