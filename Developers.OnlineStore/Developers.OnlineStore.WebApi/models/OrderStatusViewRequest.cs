﻿namespace Developers.OnlineStore.WebApi.models
{
    public class OrderStatusViewRequest:BaseSysModel
    {
        public string StatusId { get; set; }
        public string OrderId { get; set; }
        public string CreationDate { get; set; }
        public string DeliveryDate { get; set; }
    }
}
