﻿using Developers.OnlineStore.Data.Contracts;
using System.Collections.Generic;

namespace Developers.OnlineStore.WebApi.models
{
    public class OrderViewResponse:BaseSysModel
    {
        public string CustomerId { get; set; }       
        public string StoreAdressId{ get; set; }
        public string DateToDelivery { get; set; }
        public virtual ICollection<OrderStatusView> OrderStatuses { get; set; }
        public ICollection<ProductOrderView> ProductOrders { get; set; }

    }
}
