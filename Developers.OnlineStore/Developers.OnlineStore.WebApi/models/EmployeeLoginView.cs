﻿
namespace Developers.OnlineStore.WebApi.models
{
    public class EmployeeLoginView:EmployeeView
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
