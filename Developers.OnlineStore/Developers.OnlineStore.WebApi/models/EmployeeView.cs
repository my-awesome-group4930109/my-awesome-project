﻿using Developers.OnlineStore.Data.Contracts;

namespace Developers.OnlineStore.WebApi.models
{
    public class EmployeeView: BaseSysModel
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string RoleId { get; set; }
    }
}
