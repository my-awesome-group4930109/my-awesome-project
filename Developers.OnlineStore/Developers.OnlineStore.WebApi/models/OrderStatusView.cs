﻿namespace Developers.OnlineStore.WebApi.models
{
    public class OrderStatusView:BaseSysModel
    {
        public string StatusId { get; set; }
        public string CreationDate { get; set; }
        public string DeliveryDate { get; set; }
        
    }
}
