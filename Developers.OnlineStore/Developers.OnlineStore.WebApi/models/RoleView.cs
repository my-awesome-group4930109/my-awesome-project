﻿

namespace Developers.OnlineStore.WebApi.models
{
    public class RoleView: BaseSysModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
