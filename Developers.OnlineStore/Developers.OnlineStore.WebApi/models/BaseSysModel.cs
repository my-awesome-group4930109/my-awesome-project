﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Developers.OnlineStore.WebApi.models
{
    public class BaseSysModel
    {
        /// <summary>
        /// Уникальный код записи
        /// </summary>
        public string Id { get; set; }
    }
}
