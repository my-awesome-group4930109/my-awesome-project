﻿namespace Developers.OnlineStore.WebApi.models
{
    public class ProductOrderViewRequest:BaseSysModel
    {
        public string ProductId { get; set; }       
        public double Price { get; set; }
    }
}
