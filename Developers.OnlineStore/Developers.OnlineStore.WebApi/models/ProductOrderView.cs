﻿namespace Developers.OnlineStore.WebApi.models
{
    public class ProductOrderView:BaseSysModel
    {
        public string ProductId { get; set; }
        public double Price { get; set; }
    }
}
