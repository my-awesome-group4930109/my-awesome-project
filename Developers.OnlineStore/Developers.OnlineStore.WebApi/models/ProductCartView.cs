﻿namespace Developers.OnlineStore.WebApi.models
{
    public class ProductCartView
    {
        public string ProductId { get; set; }       
        public long Count { get; set; }
    }
}
