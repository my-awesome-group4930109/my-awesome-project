﻿using System.Collections;
using System.Collections.Generic;

namespace Developers.OnlineStore.WebApi.models
{
    public class CartView
    {
        public string CustomerId { get; set; }
        public virtual ICollection<ProductCartView> Products { get; set; }
    }
}
