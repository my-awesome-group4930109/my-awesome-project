﻿using AutoMapper;
using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Core.Repositories;
using Developers.OnlineStore.Data.BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using Developers.OnlineStore.WebApi.models;
using System.Collections.Generic;
using Developers.OnlineStore.Data.Contracts;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace Developers.OnlineStore.WebApi.Controllers
{
    /// <summary>
    /// Методы по работе
    /// </summary>
    [Produces("application/json")]
    [Route("Employee")]
    [ApiController]
    public class EmployeeController : BaseAPIController
    {
        //private readonly IMapper _mapper;
        private readonly IEmployeeService _employeeService;
        //private readonly IRepository<Employee> _employeeRepository;
        public  EmployeeController(IEmployeeService employeeService, IServiceProvider provider, ILogger<EmployeeController> logger, IConfiguration config, IMapper mapper) : base(provider, logger, config, mapper)
        {
            //_employeeRepository = repository;
            //_mapper = mapper;
            _employeeService = employeeService;
        }

        /// <summary>
        /// Получить сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Сотрудник</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeView>> GetEmployeeByIdAsync(Guid id)
        {
            var entity = await _employeeService.GetById(id);
            var employee = _mapper.Map<EmployeeView>(entity);
            return Ok(employee);
        }

        /// <summary>
        /// Получить список всех сотрудников по роли
        /// </summary>
        /// <returns>Список сотрудников</returns>
        [HttpGet("GetEmployeesAsync")]       
        public async Task<List<EmployeeView>> GetEmployeesAsync(Guid roleId)
        {
            var entities = await _employeeService.GetEmployees(x=>x.RoleId == roleId.ToString());
            List <EmployeeView> employees = _mapper.Map<List<EmployeeView>>(entities);
            return employees;
        }

        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        /// <param name="employeeLogin"></param>
        /// <returns>ID добавленного сотрудника</returns>
        [HttpPost]
        public async Task<IActionResult> AddEmployee(EmployeeLoginView employeeLogin)
        {
            Guid result;
            try
            {
                var employee = _mapper.Map<EmployeeDTO>(employeeLogin);
                 result = await _employeeService.AddEmployeeAsync(employee);
            }
            catch (Exception ex) 
            {
                _logger.LogError(ex, "Ошибка добавления роли AddEmployee!");
                return BadRequest(new { result = "error", errorMessage = "Ошибка добавления роли" + ex.Message });
            }
            return Ok(new { result = "ok", Message = "Добавление успешно!", newRowID = result });
        }

        /// <summary>
        /// Обновить данные сотрудника
        /// </summary>
        /// <param name="employeeLogin"></param>
        /// <returns>Сообщение об успешном добавлении</returns>
        [HttpPut("{id}")]
        public async Task <IActionResult> EditEmployee(EmployeeLoginView employeeLogin,Guid id)
        {
            var employee = _mapper.Map<EmployeeDTO>(employeeLogin);
            employee.Id = id.ToString();
            await _employeeService.UpdateEmployeeAsync(employee);                        
            return Ok("Данные сотрудника обновлены");
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployee(Guid id)
        {
            await _employeeService.DeleteEmployeeAsync(id);
            return Ok("Сотрудник удалён");
        }



    }
}
