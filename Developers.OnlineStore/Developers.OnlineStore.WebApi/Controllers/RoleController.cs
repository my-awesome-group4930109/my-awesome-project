﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using AppContext = Developers.OnlineStore.Core.Domain.StoreContext;
using Developers.OnlineStore.Data.BLL.Interfaces;
using System;
using Microsoft.Extensions.Caching.Memory;
using Developers.OnlineStore.Core.Domain;
using System.Net;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Developers.OnlineStore.Data.Contracts;
using Developers.OnlineStore.WebApi.models;

namespace Developers.OnlineStore.WebApi.Controllers
{
    /// <summary>
    /// Методы по работе с ролями
    /// </summary>
    [Produces("application/json")]
    [Route("role")]
    [ApiController]
    public class RoleController : BaseAPIController
    {
        private IRole _dataRole { get; set; }
        public RoleController(IRole role_, IServiceProvider provider, ILogger<RoleController> logger, IConfiguration config, IMapper mapper) : base(provider,  logger, config, mapper)
        {
            _dataRole = role_;
        }
        /// <summary>
        /// Список ролей
        /// </summary>
        /// <returns>лист с данными </returns>
        [HttpGet("getallrole")]
        [ProducesResponseType(typeof(RoleView), (int)HttpStatusCode.OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetAllRole()
        {
            try
            {
                var list_ = await _dataRole.GetAllRoles();
                return Ok(list_);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка получения данных GetAllRole!");
                return BadRequest(new { result = "error", errorMessage = ex.Message });
            }

        }

        /// <summary>
        /// Поиск роли
        /// </summary>
        /// <param name="RoleId">ID роли</param>
        /// <returns>JSON с данными роли </returns>
        [HttpGet("GetRole")]
        [ProducesResponseType(typeof(RoleView), (int)HttpStatusCode.OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetRole(Guid RoleId)
        {
            try
            {
                RoleView RoleFind = new();
                RoleFind = _mapper.Map<RoleView>(await _dataRole.GetRole(RoleId));
                return Ok(RoleFind);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка получения данных GetRole!");
                return BadRequest(new { result = "error", errorMessage = ex.Message });
            }

        }
        /// <summary>
        /// Получение роли  по idEmpl
        /// </summary>
        /// <param name="idEmpl">Код пользователя</param>
        /// <returns>Role</returns>
        [HttpGet("GetRoleEmpl")]
        [ProducesResponseType(typeof(RoleView), (int)HttpStatusCode.OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetRoleEmpl(Guid idEmpl)
        {
            try
            {
                RoleView list_ = _mapper.Map<RoleView>(await _dataRole.GetRoleEmpl(idEmpl));
                return Ok(list_);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка получения данных GetListRolesEmpl!");
                return BadRequest(new { result = "error", errorMessage = ex.Message });
            }
        }
        /// <summary>
        /// Добавить роль
        /// </summary>
        /// <param name="role">объект Role</param>
        /// <returns>result = "ok"</returns>
        [HttpPost]
        [ProducesResponseType(typeof(object), (int)HttpStatusCode.OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> CreateAsync([FromBody] RoleView role)
        {
            Guid newID=Guid.NewGuid();
            try
            {
                role.Id = newID.ToString();
                var newRow = _mapper.Map<RoleDTO>(role);
               await _dataRole.AddRole(newRow);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка добавления роли CreateAsync!");
                return BadRequest(new { result = "error", errorMessage = "Ошибка добавления роли" + ex.Message });
            }

            return Ok(new { result = "ok", Message = "Добавление успешно!", newRowID = newID });
        }
        /// <summary>
        /// Изменить роль
        /// </summary>
        /// <param name="role">объект Role</param>
        /// <param name="id">id записи для корректировки</param>
        /// <returns>result = "ok"</returns>
        [HttpPut]
        [ProducesResponseType(typeof(object), (int)HttpStatusCode.OK)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> UpdateAsync([FromBody] RoleView role)
        {
            try
            {
                var newRow = _mapper.Map<RoleDTO>(role);
                 await _dataRole.UpdRole(newRow);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка обновления роли UpdateAsync!");
                return BadRequest(new { result = "error", errorMessage = "Ошибка добавления роли" + ex.Message });
            }

            return Ok(new { result = "ok", Message = "Корректировка успешна!", updRowID = role.Id });
        }
        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="del">true- удалить физически; false-поставить признак удаления</param>
        /// <returns>result = "ok"</returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            try
            {
                await _dataRole.DelRole(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка при удалении роли!");
                return BadRequest(new { result = "error", errorMessage = ex.Message });
            }

            return Ok(new { result = "ok", errorMessage = $"Удалено!" });
        }

    }
}
