﻿using AutoMapper;
using Developers.OnlineStore.Core.DAL.Infrastructure;
using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Data.BLL.Interfaces;
using Developers.OnlineStore.Data.BLL.Services;
using Developers.OnlineStore.Data.Contracts;
using Developers.OnlineStore.WebApi.models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Developers.OnlineStore.WebApi.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("Order")]
    public class OrderController : BaseAPIController
    {
        private readonly IOrderService _orderService;
        public OrderController(IOrderService orderService, IServiceProvider provider, ILogger <OrderController> logger, IConfiguration config, IMapper mapper) : base(provider, logger, config, mapper)
        {
            _orderService = orderService;
        }

        /// <summary>
        /// Получить заказ по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>orderView</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<OrderViewResponse>> GetOrderById(Guid id)
        {
            var orderDTO = await _orderService.GetOrderById(id);
            if (orderDTO == null)
            {
                return NotFound("Загрузить данные заказа не удалось");
            }
            var orderView = _mapper.Map<OrderViewResponse>(orderDTO);          
            return Ok(orderView);
        }

        /// <summary>
        /// Получить все заказы покупателя
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("Customer/{id:guid}")]
        public async Task<ActionResult<OrderViewResponse>> GetOrdersByCustomerId(Guid id)
        {
            var ordersDTO = await _orderService.GetOrdersByCustomerID(id);
            if (ordersDTO == null)
            {
                return NotFound("Загрузить заказы пользователя не удалось");
            }
            var ordersView=_mapper.Map<List<OrderViewResponse>>(ordersDTO);
            return Ok(ordersView);
        }

        /// <summary>
        /// Получить все заказы
        /// </summary>
        /// <returns></returns>
        [HttpGet("All")]
        public async Task<ActionResult<OrderShortResponse>> GetAllOrders()
        {
            var ordersDTO = await _orderService.GetAllOrders();
            if (ordersDTO == null)
            {
                return NotFound("Загрузить заказы не удалось");
            }
            var ordersView = _mapper.Map<List<OrderShortResponse>>(ordersDTO);
            return Ok(ordersView);
        }
        /// <summary>
        /// Создать новый заказ
        /// </summary>
        /// <param name="request"></param>
        /// <returns>orderDTO.Id</returns>
        /// <exception cref="ValidationException"></exception>
        [HttpPost("Add")]
        public async Task<ActionResult<Guid>> CreateNewOrder(OrderViewRequest request)
        {
            var orderDTO=_mapper.Map<OrderDTO>(request);
            Guid result;
            try
            {
                 result = await _orderService.AddOrderAsync(orderDTO);
            }
            catch
            {
                throw new ValidationException("Создать заказ не удалось", "");
            }
          
            return Ok(result);
        }
        /// <summary>
        /// Обновить данные заказа
        /// </summary>
        /// <param name="request"></param>
        /// <param name="id"></param>
        /// <returns>OrderViewResponse</returns>
        [HttpPut("Update")]
        public async Task<ActionResult<OrderViewResponse>> EditOrder(OrderViewRequest request,Guid id)
        {
            var orderDTO = _mapper.Map<OrderDTO>(request);
            orderDTO.Id = id.ToString();
            var newOrder = await _orderService.UpdateOrderAsync(orderDTO);
            var result = _mapper.Map<OrderViewResponse>(newOrder); 
            return Ok(result);
        }

        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> DeleteOrder(Guid id)
        {
            await _orderService.DeleteOrderAsync(id);
            return Ok("Заказ удалён");
        }




    }
}
