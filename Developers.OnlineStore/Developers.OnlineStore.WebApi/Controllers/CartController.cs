﻿using AutoMapper;
using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Core.Repositories;
using Developers.OnlineStore.Data.BLL.Interfaces;
using Developers.OnlineStore.Data.BLL.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using Developers.OnlineStore.WebApi.models;
using System.Collections.Generic;
using System.Linq;
using Developers.OnlineStore.Data.Contracts;
using Developers.OnlineStore.Core.DAL.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Developers.OnlineStore.WebApi.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("Cart")]
    public class CartController : BaseAPIController
    {
        
        private readonly ICartService _cartService;


        public CartController(ICartService cartService, IServiceProvider provider, ILogger<CartController> logger, IConfiguration config, IMapper mapper) : base(provider, logger, config, mapper)
        {
           
            _cartService = cartService;
        }

        /// <summary>
        /// Получить содержимое корзины
        /// </summary>
        /// <param name="id"></param>
        /// <returns>CartView</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CartView>> GetCartByIdAsync(Guid id)
        {
            
            var cartDTO = await _cartService.GetCart(id);
            var cartView = _mapper.Map<CartView>(cartDTO);
            if (cartDTO == null)
            {
                return NotFound("Загрузить корзину не удалось");
            }  
            return Ok(cartView);
        }
        /// <summary>
        /// Сохранить товары в корзине
        /// </summary>
        /// <param name="cartView"></param>
        /// <returns>CartView</returns>
        /// <exception cref="ValidationException"></exception>
        [HttpPost("Add")]
        public async Task<ActionResult> AddCart(CartView cartView)
        {
            var cartDTO = _mapper.Map<CartDTO>(cartView);
        
            try
            {
                await _cartService.AddCart(cartDTO);
            }
            catch
            {
                throw new ValidationException("Сохранить корзину не удалось", "");
            }
            
            return Ok("Список товаров в корзине сохранен");
        }

        /// <summary>
        /// Обновить список товаров в корзине
        /// </summary>
        /// <param name="cartView"></param>
        /// <returns>CartView</returns>
        [HttpPut("Update/{id}")]
        public async Task<ActionResult<CartView>> EditCart(CartView cartView, Guid id)
        {
            var cartDTO = _mapper.Map<CartDTO>(cartView);
            cartDTO.Id = id.ToString();            
            var newCart = await _cartService.UpdateCart(cartDTO);                      
            var result = _mapper.Map<CartView>(newCart);
            return Ok(result);
        }
        /// <summary>
        /// Удалить товары из корзины
        /// </summary>
        /// <param name="id"></param>        
        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> DeleteCart(Guid id)
        {
            await _cartService.DeleteCart(id);
            return Ok("Данные корзины удалены");
        }
    }
}
