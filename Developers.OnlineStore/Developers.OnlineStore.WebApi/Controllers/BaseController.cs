﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Developers.OnlineStore.WebApi.Controllers
{
    public class BaseAPIController : ControllerBase
    {
        protected ILogger _logger;
        protected IConfiguration _configuration;
        //protected readonly IMemoryCache _memoryCache;
        protected readonly IServiceProvider _serviceProvider;
        protected readonly IMapper _mapper;

        public BaseAPIController(IServiceProvider provider, /*IMemoryCache memoryCache,*/ ILogger logger, IConfiguration config, IMapper mapper) : base()
        {
            //_memoryCache = memoryCache;
            _logger = logger;
            _configuration = config;
            _serviceProvider = provider;
            _mapper = mapper;
        }
    }
}
