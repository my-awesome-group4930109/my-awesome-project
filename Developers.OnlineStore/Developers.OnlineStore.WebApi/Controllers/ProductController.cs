﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using AppContext = Developers.OnlineStore.Core.Domain.StoreContext;
using System.Threading.Tasks;
using Developers.OnlineStore.Data.BLL.Interfaces;
using System.Net;
using Developers.OnlineStore.WebApi.models;
using AutoMapper;
using Developers.OnlineStore.Data.Contracts;

namespace Developers.OnlineStore.WebApi.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("product")]
    public class ProductController : Controller
    {
        IProduct _product;
        IMapper _mapper;
        public ProductController(ILogger<ProductController> logger, IConfiguration config, AppContext context,IProduct product, IMapper mapper)
        {
            _mapper = mapper;
            _product = product;
        }
        [HttpPost("addproduct")]
        [ProducesResponseType(typeof(ProductViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ProductViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult> AddProduct([FromBody] ProductDTO prd)
        {
            if (prd != null)
                return Ok(_mapper.Map<ProductViewModel>(_product.AddProduct(prd).Result));
            else
                return BadRequest(new { result = "error", errorMessage = "Ошибка запроса!" });
        }
        [HttpGet]
        [ProducesResponseType(typeof(ProductViewModel), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> GetProduct(Guid id)
        {
            var product = _product.GetProduct(id);
            if (product == null)
                return NotFound("Продукт не найден");
            return Ok(product);
        }
    }
}
