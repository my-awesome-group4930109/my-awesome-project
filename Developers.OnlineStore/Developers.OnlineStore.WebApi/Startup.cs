using AutoMapper;
using Developers.OnlineStore.Core.Repositories;
using Developers.OnlineStore.Core.Services;
using Developers.OnlineStore.Data.BLL.Interfaces;
using Developers.OnlineStore.Data.BLL.Services;
using Developers.OnlineStore.Data.Mapping;
using Developers.OnlineStore.WebApi.Automapper;
using Developers.OnlineStore.WebApi.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Developers.OnlineStore.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connection = Configuration.GetSection("ConnectionString").Value;
            services.AddTransient(typeof(IRepository<>), typeof(RepositoryGeneric<>));            
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Developers.OnlineStore.WebApi", Version = "v1" });
            });
            services.AddDataAccessModule(configuration =>     configuration.IServiceCollection(connection) );
            services.AddAutomapperModule();

            services.AddCors(opt => opt.AddDefaultPolicy(
              builder => builder
                  .WithOrigins(Configuration["Cors"].Split(";"))
                  .AllowAnyHeader()
                  .AllowAnyMethod()
                  .AllowCredentials()
          ));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Developers.OnlineStore.WebApi v1"));
            }
            app.UseGlobalExceptionHandler();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors();

            app.UseAuthorization();

            app.UseLoggingMiddleware();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
