﻿using AutoMapper;
using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Data.Contracts;
using Developers.OnlineStore.WebApi.models;
using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace Developers.OnlineStore.WebApi.mapping
{
    public class CartMappingProfile:Profile
    {
        public CartMappingProfile() 
        {
            CreateMap<Cart, CartDTO>();
            CreateMap<ProductCart,ProductCartDTO>();

            CreateMap<CartDTO, Cart>()
                .ForMember(d => d.Customer, map => map.Ignore());
            CreateMap<ProductCartDTO, ProductCart>()
                .ForMember(d => d.Cart, map => map.Ignore())
                .ForMember(d => d.Product, map => map.Ignore());

            CreateMap<CartDTO, CartView>()
                .ForMember(d => d.Products, map => map.MapFrom(src=>src.ProductCarts));
            
            CreateMap<ProductCartDTO, ProductCartView>()
                .ForMember(d => d.ProductId, map => map.MapFrom(c => c.ProductId))
                .ForMember(d => d.Count, map => map.MapFrom(c => c.Count));

            CreateMap<CartView, CartDTO>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.ProductCarts, map =>map.MapFrom(s=>s.Products));
                
            CreateMap<ProductCartView, ProductCartDTO>()
                .ForMember(d => d.ProductId, map => map.MapFrom(p => p.ProductId))
                .ForMember(d => d.Count, map => map.MapFrom(c => c.Count))
                .ForMember(d => d.CartId, map => map.Ignore())
                .ForMember(d => d.Id, map => map.Ignore());
                
        }
    }
}
