﻿using AutoMapper;
using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Data.Contracts;
using Developers.OnlineStore.WebApi.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Developers.OnlineStore.WebApi.Automapper.Profiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<Product, ProductDTO>()./*ForMember(d => d., map => map.Ignore()).*/IncludeAllDerived();
            CreateMap<ProductDTO, Product>()
                .ForMember(d => d.Category, map => map.Ignore())
                .ForMember(d => d.CompatibleProduct, map => map.Ignore())
                .ForMember(d => d.ProductCarts, map => map.Ignore())
                .ForMember(d => d.ProductImages, map => map.Ignore())
                .ForMember(d => d.ProductOrders, map => map.Ignore())
                .ForMember(d => d.Storages, map => map.Ignore())
                .ForMember(d => d.Comments, map => map.Ignore())
                .IncludeAllDerived();
            CreateMap<ProductDTO, ProductViewModel>().IncludeAllDerived();
        }
    }
}
