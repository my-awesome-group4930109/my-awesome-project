﻿using AutoMapper;
using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Data.Contracts;
using Developers.OnlineStore.WebApi.models;

namespace Developers.OnlineStore.WebApi.mapping
{
    public class RoleProfile : Profile
    {
        public RoleProfile()
        {
            CreateMap<Role, RoleDTO>().IncludeAllDerived();
            CreateMap<RoleDTO, Role>().ForMember(d => d.Employees, map => map.Ignore()).IncludeAllDerived();
            CreateMap<RoleDTO,RoleView >().IncludeAllDerived();
            CreateMap<RoleView,RoleDTO>().IncludeAllDerived();
        }
    }
}
