﻿using AutoMapper;
using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Data.Contracts;
using Developers.OnlineStore.WebApi.models;

namespace Developers.OnlineStore.Data.Mapping
{
    public class EmployeeMappingProfile:Profile
    {
        public EmployeeMappingProfile() 
        {
            CreateMap<Employee, EmployeeDTO>();
            CreateMap<EmployeeDTO, Employee>()                      
                .ForMember(d=>d.Role,map=>map.Ignore());

            //CreateMap<Role, RoleDTO>();
            CreateMap<EmployeeDTO, EmployeeView>();
            CreateMap<EmployeeView, EmployeeDTO>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.Login, map => map.Ignore())
                .ForMember(d => d.Password, map => map.Ignore());




            //CreateMap<RoleDTO, RoleView>();
            //CreateMap<RoleView, RoleDTO>().
            //    ForMember(p => p.Employees, map => map.Ignore());
            CreateMap<EmployeeLoginView, EmployeeDTO>()
                .ForMember(d => d.Id, map => map.Ignore());
        }
    }
}
