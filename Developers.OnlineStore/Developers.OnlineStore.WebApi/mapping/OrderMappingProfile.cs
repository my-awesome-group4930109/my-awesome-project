﻿using AutoMapper;
using Developers.OnlineStore.Core.Domain;
using Developers.OnlineStore.Data.Contracts;
using Developers.OnlineStore.WebApi.models;
using System.Linq;

namespace Developers.OnlineStore.WebApi.mapping
{
    public class OrderMappingProfile:Profile
    {
        public OrderMappingProfile()
        {
            //from orderDTO to ViewResponse
            CreateMap<OrderDTO, OrderViewResponse>()
                .ForPath(d => d.OrderStatuses, map => map.MapFrom(s => s.OrderStatuses))                
                .ForMember(d => d.ProductOrders, map => map.MapFrom(s => s.ProductOrders));
            CreateMap<OrderStatusDTO, OrderStatusView>();
            CreateMap<ProductOrderDTO, ProductOrderView>();




            //fromViewRequest to orderDto
            CreateMap<OrderViewRequest, OrderDTO>()
                .ForMember(d => d.Customer, map => map.Ignore())
                .ForMember(d => d.StoreAdress, map => map.Ignore())
                .ForMember(d => d.OrderStatuses, map => map.MapFrom(src => src.OrderStatuses))
                .ForMember(d => d.ProductOrders, map => map.MapFrom(src => src.ProductOrders));

            CreateMap<ProductOrderViewRequest, ProductOrderDTO>()
                .ForMember(d => d.OrderId, map => map.Ignore());
            CreateMap<OrderStatusView, OrderStatusDTO>()
                .ForMember(d => d.OrderId, map => map.Ignore())
                .ForMember(d => d.Status, map => map.Ignore())
                .ForMember(d => d.Order, map => map.Ignore());

            //from orderDTO to order
            CreateMap<OrderDTO, Order>()
                .ForMember(d => d.OrderStatuses, map => map.MapFrom(src => src.OrderStatuses))
                .ForMember(d => d.ProductOrders, map => map.MapFrom(src => src.ProductOrders))
                .ReverseMap();


            CreateMap<OrderStatusDTO, OrderStatus>().ReverseMap();
            CreateMap<ProductOrderDTO,ProductOrder>()
                .ForMember(d=>d.Product,map=>map.Ignore())
                .ForMember(d => d.Order, map => map.Ignore());

            //from Order to OrderDTO
            CreateMap<Order, OrderDTO>();
            CreateMap<ProductOrder, ProductOrderDTO>();

            //from orderDTO to OrderShortRequest
            CreateMap<OrderDTO, OrderShortResponse>()
                .ForMember(d => d.StatusId, map => map.MapFrom(x => x.OrderStatuses.Select(x => x.StatusId).FirstOrDefault().ToString()));
        }
    }
}
