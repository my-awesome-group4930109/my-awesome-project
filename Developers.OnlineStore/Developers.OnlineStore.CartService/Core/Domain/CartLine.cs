﻿using System;

namespace Developers.OnlineStore.CartService.Core.Domain
{
    /// <summary>
    /// Позиция в заказе.
    /// </summary>
    public class CartLine:IEntity
    {
        /// <summary>
        /// ID продукта в позиции.
        /// </summary>
        public Guid ProductId { get; set; }
        /// <summary>
        /// ID корзины.
        /// </summary>
        public Guid CartId { get; set; }
        /// <summary>
        /// Колличество товара в позиции.
        /// </summary>
        public long Count { get; set; }

        public virtual Cart Cart { get; set; }
        
    }
}
