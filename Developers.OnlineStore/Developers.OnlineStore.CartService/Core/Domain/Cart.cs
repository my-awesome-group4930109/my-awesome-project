﻿using System.Collections.Generic;

namespace Developers.OnlineStore.CartService.Core.Domain
{
    /// <summary>
    /// Корзина.
    /// </summary>
    public class Cart:IEntity
    {
        /// <summary>
        /// Общая стоимость товаров в корзине.
        /// </summary>
        public float TotalPrice { get; set; }
        /// <summary>
        /// Коллекция позиций в корзине.
        /// </summary>
        public virtual ICollection<CartLine> CartLines { get; set; }
    }
}
