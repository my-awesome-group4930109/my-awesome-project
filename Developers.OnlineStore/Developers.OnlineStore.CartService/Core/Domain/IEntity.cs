﻿using System;

namespace Developers.OnlineStore.CartService.Core.Domain
{
    /// <summary>
    /// Базовая сущность.
    /// </summary>
    public class IEntity
    {
        public Guid Id { get;set; }
    }
}
