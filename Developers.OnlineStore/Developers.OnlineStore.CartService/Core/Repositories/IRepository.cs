﻿using Developers.OnlineStore.CartService.Core.Domain;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;

namespace Developers.OnlineStore.CartService.Core.Repositories
{
    public interface IRepository<T> where T : IEntity
    {
        /// <summary>
        /// Получить сущность из базы данных по фильтру.
        /// </summary>
        /// <param name="filter">Фильтр поиска по базе данных.</param>
        /// <param name="includeProperties">Опция загрузки данных из связанных таблиц.</param>
        /// <returns>Сущность данного типа.</returns>
        Task<T> GetByFilterAsync(Expression<Func<T, bool>> filter, params Expression<Func<T, object>>[] includeProperties);
        /// <summary>
        /// Добавить в базу данных новый экземпляр сущности.
        /// </summary>
        /// <param name="entity">Базовая сущность.</param>
        Task AddAsync(T entity);        
        /// <summary>
        /// Изменить данные сущности в базе данных.
        /// </summary>
        /// <param name="entity">Базовая сущность.</param>
        Task UpdateAsync(T entity);
        /// <summary>
        /// Удалить сущность из базы данных.
        /// </summary>
        /// <param name="filter">Параметры поиска сущности в базе данных.</param>
        Task RemoveAsync(Expression<Func<T, bool>> filter);        
        /// <summary>
        /// Сохранить изменения в базе данных.
        /// </summary>
        Task SaveChangesAsync();
    }
}
