using Developers.OnlineStore.CartService.Core.Repositories;
using Developers.OnlineStore.CartService.Data.Repositories.Implementation;
using Developers.OnlineStore.CartService.Data.Services.Interfaces;
using Developers.OnlineStore.CartService.Data.Services.Implementation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Developers.OnlineStore.CartService.Integration;
using MassTransit;

namespace Developers.OnlineStore.CartService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddScoped(typeof(IRepository<>), typeof(GenericRepository<>));
            services.AddScoped<ICartService,CartServices>();
            services.AddScoped<IPushCartToOrderBrocker, PushCartToOrderBrocker>();
            services.AddMassTransit(x => {
                x.UsingRabbitMq((context, cfg) =>
                {
                    RabbitConfigure(cfg);
                });
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Developers.OnlineStore.CartService", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Developers.OnlineStore.CartService v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        private static void RabbitConfigure(IRabbitMqBusFactoryConfigurator configurator)
        {            
            configurator.Host("localhost",
                h =>
                {
                    h.Username("rmuser");
                    h.Password("rmpassword");
                });
        }
    }
}
