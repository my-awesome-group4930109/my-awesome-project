﻿using AutoMapper;
using Developers.OnlineStore.CartService.Core.Domain;
using Developers.OnlineStore.CartService.Core.Repositories;
using Developers.OnlineStore.CartService.Data.Models;
using Developers.OnlineStore.CartService.Data.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Developers.OnlineStore.CartService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly IRepository<Cart> _repository;
        private readonly ICartService _cartService;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        public CartController(IRepository<Cart> repository, ICartService cartService, IMapper mapper, ILogger<CartController> logger)
        {
            _repository = repository;
            _cartService = cartService;
            _mapper = mapper;
            _logger = logger;
        }
        /// <summary>
        /// Получить корзину по ID.
        /// </summary>
        /// <param name="id">ID корзины.</param>
        /// <returns>Модель корзины.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CartModel>> GetCartByID(Guid id)
        {
            var result = await _cartService.GetCart(id);
            return result;
        }
        /// <summary>
        /// Создать новый экземпляр корзины.
        /// </summary>
        /// <param name="cartModel">Модель корзины.</param>
        /// <returns>ID созданной коризны.</returns>
        [HttpPost("Add")]
        public async Task<ActionResult<Guid>> AddNewCart(CartModel cartModel)
        {
            var result = await _cartService.AddCart(cartModel);
            return result;
        }
        /// <summary>
        /// Добавить новую позицию в заказ.
        /// </summary>
        /// <param name="cartId">ID существующей корзины.</param>
        /// <param name="cartLineModel">ID позиции.</param>
        /// <returns>Обновленная корзина.</returns>
        [HttpPut()]
        public async Task<ActionResult<CartModel>> AddNewCartLine(Guid cartId, CartLineModel cartLineModel)
        {
            var result = await _cartService.AddCartLine(cartId, cartLineModel);
            return result;
        }
        /// <summary>
        /// Изменить количество товара в позиции.
        /// </summary>
        /// <param name="cartId">ID корзины.</param>
        /// <param name="cartLineId">ID позиции.</param>
        /// <param name="count">Новое количество товара.</param>
        /// <returns>Обновленная корзина.</returns>
        [HttpPut()]
        public async Task <ActionResult<CartModel>> ChangeCartLineCount(Guid cartId, Guid cartLineId, int count)
        {
            var result = await _cartService.ChangeCartLineCount(cartId, cartLineId, count);
            return result;
        }
        /// <summary>
        /// Удалить позицию из заказа.
        /// </summary>
        /// <param name="cartId">ID коризны.</param>
        /// <param name="cartLineId">ID позиции в корзине.</param>
        /// <returns>Обновленная корзина.</returns>
        [HttpPut()]
        public async Task <ActionResult<CartModel>> DeleteCartLineFromCart(Guid cartId, Guid cartLineId)
        {
            var result = await _cartService.DeleteCartLineFromCart(cartId, cartLineId);
            return result;
        }
        /// <summary>
        /// Удалить корзину.
        /// </summary>
        /// <param name="id">ID корзины.</param>
        [HttpDelete("id")]
        public async Task <ActionResult> DeleteCart(Guid id)
        {
            await _cartService.DeleteCart(id);
            return Ok();
        }

    }
}
