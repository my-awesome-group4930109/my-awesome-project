﻿using Developers.OnlineStore.CartService.Data.Models;
using Developers.OnlineStore.Message;
using MassTransit;
using System.Threading.Tasks;

namespace Developers.OnlineStore.CartService.Integration
{
    public class PushCartToOrderBrocker : IPushCartToOrderBrocker
    {
        private readonly IBusControl _busControl;
        public PushCartToOrderBrocker(IBusControl busControl)
        {
            _busControl = busControl;
        }
        public async Task PushCart(CartModel cartModel)
        {
            var dto = new CartDTO()
            {
                Id = cartModel.Id,
                TotalPrice = cartModel.TotalPrice
            };
            await _busControl.Publish<CartDTO>(dto);
        }
    }
}
