﻿using System;

namespace Developers.OnlineStore.Message
{
    /// <summary>
    /// Класс корзины для отправки сообщения.
    /// </summary>
    public class CartDTO
    {
        /// <summary>
        /// ID модели корзины.
        /// </summary>
        public Guid Id { get; set; }
        // <summary>
        /// Общая стоимость товаров в корзине.
        /// </summary>
        public float TotalPrice { get; set; }
    }
}
