﻿using Developers.OnlineStore.CartService.Data.Services.Interfaces;
using Developers.OnlineStore.Data.Contracts;
using MassTransit;
using System.Threading.Tasks;

namespace Developers.OnlineStore.CartService.Integration.Consumer
{
    public class ProductConsumer:IConsumer<ProductDTO>
    {
        private readonly ICartService _cartService;
        public ProductConsumer(ICartService cartService)
        {
            _cartService = cartService;
        }
        public async Task Consume(ConsumeContext<ProductDTO> context)
        {
            //TODO: Настроить маршрутизацию сервисов.
        }
    }
}
