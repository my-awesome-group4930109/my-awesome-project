﻿using Developers.OnlineStore.CartService.Data.Models;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.Threading.Tasks;

namespace Developers.OnlineStore.CartService.Integration
{
    /// <summary>
    /// Сервис отправки содержимого корзины в сервис оформления заказа.
    /// </summary>
    public interface IPushCartToOrderBrocker
    {
        Task PushCart(CartModel cartModel);
    }
}
