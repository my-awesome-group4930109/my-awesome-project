﻿using Developers.OnlineStore.CartService.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace Developers.OnlineStore.CartService.Data
{
    /// <summary>
    /// Класс контекста данных БД.
    /// </summary>
    public class DataContext:DbContext
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }
        public DbSet<Cart> Carts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}
