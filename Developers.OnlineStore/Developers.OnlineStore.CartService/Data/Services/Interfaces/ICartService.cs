﻿using System.Threading.Tasks;
using System;
using Developers.OnlineStore.CartService.Data.Models;

namespace Developers.OnlineStore.CartService.Data.Services.Interfaces
{
    /// <summary>
    /// Интерфейс сервиса работы с корзиной.
    /// </summary>
    public interface ICartService
    {
        /// <summary>
        /// Получить корзину с товарами.
        /// </summary>
        /// <param name="id">ID корзины.</param>
        /// <returns>Корзина с товарами.</returns>
        Task<CartModel> GetCart(Guid id);
        /// <summary>
        /// Создать корзину с товарами.
        /// </summary>
        /// <param name="cartModel">Модель корзины.</param>
        /// <returns>ID корзины.</returns>
        Task<Guid> AddCart(CartModel cartModel);
        /// <summary>
        /// Удалить существующую корзину
        /// </summary>
        /// <param name="id">ID корзины.</param>
        Task DeleteCart(Guid id);
        /// <summary>
        /// Добавить позицию в корзину.
        /// </summary>
        /// <param name="cartId">ID корзины.</param>
        /// <param name="cartLineModel">Позиция заказа.</param>
        /// <returns>Обновленная модель корзины.</returns>
        Task<CartModel> AddCartLine(Guid cartId, CartLineModel cartLineModel);
        /// <summary>
        /// Изменить количества товара в позиции
        /// </summary>
        /// <param name="cartId">ID корзины.</param>
        /// <param name="cartLineId">ID позиции заказа.</param>
        /// <param name="count">Количество товара в позиции.</param>
        /// <returns>Обновленная модель корзины.</returns>
        Task<CartModel> ChangeCartLineCount(Guid cartId,Guid cartLineId,int count);
        /// <summary>
        /// Удалить позицию из заказа.
        /// </summary>
        /// <param name="cartId"></param>
        /// <param name="cartLineId"></param>
        /// <returns>Обновленная модель корзины.</returns>
        Task<CartModel> DeleteCartLineFromCart(Guid cartId, Guid cartLineId);     

    }
}
