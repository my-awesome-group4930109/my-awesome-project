﻿using AutoMapper;
using Developers.OnlineStore.CartService.Core.Domain;
using Developers.OnlineStore.CartService.Core.Repositories;
using Developers.OnlineStore.CartService.Data.Models;
using Developers.OnlineStore.CartService.Data.Services.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Developers.OnlineStore.CartService.Data.Services.Implementation
{
    /// <summary>
    /// Сервис для работы с корзиной.
    /// </summary>
    public class CartServices : ICartService
    {
        private readonly IRepository<Cart> _repository;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public CartServices(IRepository<Cart> repository, IMapper mapper, ILogger logger)
        {
            _repository = repository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Guid> AddCart(CartModel cartModel)
        {
            var entity = _mapper.Map<Cart>(cartModel);
            Guid id = Guid.NewGuid();
            entity.Id = id;
            foreach (var line in entity.CartLines)
            {
                line.CartId = id;
                line.Id = Guid.NewGuid();
            }
            await _repository.AddAsync(entity);
            return entity.Id;
        }

        public async Task<CartModel> AddCartLine(Guid cartId, CartLineModel cartLineModel)
        {
            var entity = await _repository.GetByFilterAsync(x=>x.Id == cartId);
            var cartLine = _mapper.Map<CartLine>(cartLineModel);
            Guid id = Guid.NewGuid();
            cartLine.Id = id;
            entity.CartLines.Add(cartLine);
            await _repository.UpdateAsync(entity);
            var newEntity = await _repository.GetByFilterAsync(x=>x.Id==cartId);
            var result = _mapper.Map<CartModel>(newEntity);
            return result;
        }

        public async Task<CartModel> ChangeCartLineCount(Guid cartId, Guid cartLineId, int count)
        {
            var entity = await _repository.GetByFilterAsync(x => x.Id == cartId);
            foreach(var line in entity.CartLines)
            {
                if(line.Id == cartLineId)
                {
                    line.Count=count;
                }
            }
            await _repository.UpdateAsync(entity);
            var newEntity = await _repository.GetByFilterAsync(x => x.Id == cartId);
            var result = _mapper.Map<CartModel>(newEntity);
            return result;
        }
        public async Task<CartModel> DeleteCartLineFromCart(Guid cartId, Guid cartLineId)
        {
            var entity = await _repository.GetByFilterAsync(x => x.Id == cartId);
            foreach (var line in entity.CartLines)
            {
                if (line.Id == cartLineId)
                {
                    entity.CartLines.Remove(line);
                }
            }
            await _repository.UpdateAsync(entity);
            var newEntity = await _repository.GetByFilterAsync(x => x.Id == cartId);
            var result = _mapper.Map<CartModel>(newEntity);
            return result;
        }
        public async Task DeleteCart(Guid id)
        {
            await _repository.RemoveAsync(x=>x.Id==id); 
        }        

        public async Task<CartModel> GetCart(Guid id)
        {
            var entity = await _repository.GetByFilterAsync(x => x.Id == id);                      
            var cartModel = _mapper.Map<CartModel>(entity);
            return cartModel;
        }
    }
}
