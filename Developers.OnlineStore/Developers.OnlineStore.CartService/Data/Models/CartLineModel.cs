﻿using System;

namespace Developers.OnlineStore.CartService.Data.Models
{
    /// <summary>
    /// Модель позиции заказа.
    /// </summary>
    public class CartLineModel
    {
        /// <summary>
        /// ID модели позиции заказа.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// ID продукта в позиции.
        /// </summary>
        public Guid ProductId { get; set; }
        /// <summary>
        /// ID корзины.
        /// </summary>
        public Guid CartId { get; set; }
        /// <summary>
        /// Колличество товара в позиции.
        /// </summary>
        public long Count { get; set; }
    }
}
