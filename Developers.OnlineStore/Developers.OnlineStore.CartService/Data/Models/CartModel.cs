﻿using Developers.OnlineStore.CartService.Core.Domain;
using System;
using System.Collections.Generic;

namespace Developers.OnlineStore.CartService.Data.Models
{
    /// <summary>
    /// Модель корзины.
    /// </summary>
    public class CartModel
    {
        /// <summary>
        /// ID модели корзины.
        /// </summary>
        public Guid Id { get; set; }
        // <summary>
        /// Общая стоимость товаров в корзине.
        /// </summary>
        public float TotalPrice { get; set; }
        /// <summary>
        /// Коллекция позиций в корзине.
        /// </summary>
        public virtual ICollection<CartLineModel> CartLines { get; set; }
    }
}
