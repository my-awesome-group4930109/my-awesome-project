﻿using AutoMapper;
using Developers.OnlineStore.CartService.Core.Domain;
using Developers.OnlineStore.CartService.Data.Models;
using System.Runtime.InteropServices;

namespace Developers.OnlineStore.CartService.Data.Mapping
{
    /// <summary>
    /// Настройка маппинга в сервисе корзины.
    /// </summary>
    public class CartMappingProfile:Profile
    {
        public CartMappingProfile()
        { 
            CreateMap<Cart,CartModel>();
            CreateMap<CartLine,CartLineModel>();

            CreateMap<CartModel, Cart>();
            CreateMap<CartLineModel,CartLine>()
                .ForMember(d=>d.Cart,map =>map.Ignore());
                

        }
    }
}
