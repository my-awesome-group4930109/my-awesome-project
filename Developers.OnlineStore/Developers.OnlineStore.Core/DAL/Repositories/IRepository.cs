﻿using Developers.OnlineStore.Core.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq.Expressions;

namespace Developers.OnlineStore.Core.Repositories
{
    public interface IRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties);

        Task<IEnumerable<T>> GetByFilterAsync(Expression<Func<T, bool>> filter, params Expression<Func<T, object>>[] includeProperties);

        Task<T> GetByIdAsync(Guid id, params Expression<Func<T, object>>[] includeProperties);

        Task<bool> AnyAsync(Guid id);
        Task<bool> AnyAsync(Expression<Func<T, bool>> filter);

        Task AddAsync(T entity);
        Task AddRangeAsync(IEnumerable<T> entities);

        Task UpdateAsync(T entity);

        Task RemoveAsync(Guid id);
        Task RemoveAsync(Expression<Func<T, bool>> filter);
        Task<IEnumerable<T>> FindListWithCount(int count = 10);
    }
}
