﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Developers.OnlineStore.Core.Repositories;
using Developers.OnlineStore.Core.Services;
using Microsoft.EntityFrameworkCore.Storage;
using AppContext = Developers.OnlineStore.Core.Domain.StoreContext;

namespace Developers.OnlineStore.Data.Services
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private bool _disposed = false;
        private Dictionary<Type, object> _repositories;
        private readonly AppContext _databaseContext;
        public UnitOfWork(AppContext databaseContext)
        {
            _databaseContext = databaseContext;
        }
        public IRepository<T> GetRepository<T>() where T : class
        {
            _repositories ??= new Dictionary<Type, object>();

            var type = typeof(T);
            if (!_repositories.ContainsKey(type))
            {
                _repositories[type] = new RepositoryGeneric<T>(_databaseContext);
            }

            return (IRepository<T>)_repositories[type];
        }
        public Task<IDbContextTransaction> BeginTransactionAsync() => _databaseContext.Database.BeginTransactionAsync();

        public Task<int> SaveChangesAsync() => _databaseContext.SaveChangesAsync();

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _databaseContext.Dispose();
                }
                this._disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
