﻿using Developers.OnlineStore.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Developers.OnlineStore.Core.DAL.Infrastructure;
using AppContext =Developers.OnlineStore.Core.Domain.StoreContext;


namespace Developers.OnlineStore.Core.Services
{
    public class RepositoryGeneric<T> : IRepository<T>
        where T : class
    {
        private readonly AppContext _databaseContext;
        private readonly DbSet<T> _dbSet;
        public RepositoryGeneric(AppContext databaseContext)
        {
            _databaseContext = databaseContext;
            _dbSet = _databaseContext.Set<T>();
        }
        private static IQueryable<T> Include(IQueryable<T> query,
         params Expression<Func<T, object>>[] includeProperties)
        {
            if (includeProperties == null) return query;

            return includeProperties
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }

        private static IQueryable<T> GetQueryable(DbSet<T> dbSet,
            params Expression<Func<T, object>>[] includeProperties)
            => Include(dbSet.AsNoTracking(), includeProperties);

        public async Task<IEnumerable<T>> GetAllAsync(
            params Expression<Func<T, object>>[] includeProperties)
            => await GetQueryable(_dbSet, includeProperties).ToListAsync();

        public async Task<IEnumerable<T>> GetByFilterAsync(Expression<Func<T, bool>> filter,
            params Expression<Func<T, object>>[] includeProperties)
            => await GetQueryable(_dbSet, includeProperties).Where(filter).ToListAsync();

        public Task<T> GetByIdAsync(Guid id,
            params Expression<Func<T, object>>[] includeProperties)
        {
            List<T> _data = new(GetQueryable(_dbSet, includeProperties));
            return Task.FromResult(_data.FirstOrDefault(e => Guid.Parse(e.GetType().GetProperty("Id").GetValue(e).ToString()) == id));
        }
        public Task<bool> AnyAsync(Guid id)
        {
            //return await _dbSet.FindAsync(id);
            List<T> _data = new(_dbSet.AsNoTracking());
            return Task.FromResult(_data.Any(e => Guid.Parse(e.GetType().GetProperty("Id").GetValue(e).ToString()) == id));
        }

        public Task<bool> AnyAsync(Expression<Func<T, bool>> filter)
            => _dbSet.AsNoTracking().AnyAsync(filter);

        public async Task AddAsync(T entity)
        {
            //if (await AnyAsync(Guid.Parse(entity.GetType().GetProperty("Id").GetValue(entity).ToString())))
            //{
            //    ThrowConflictException();
            //}

            await _dbSet.AddAsync(entity);
            await _databaseContext.SaveChangesAsync();
        }

        public async Task AddRangeAsync(IEnumerable<T> entities)
        {
            var ids = entities.ToList().Select(e => Guid.Parse(e.GetType().GetProperty("Id").GetValue(e).ToString()));

            if (await _dbSet.AnyAsync(x => ids.Contains(Guid.Parse(x.GetType().GetProperty("Id").GetValue(x).ToString()))))
            {
                ThrowConflictException();
            }

            await _dbSet.AddRangeAsync(entities);
            await _databaseContext.SaveChangesAsync();
        }

        public async Task RemoveAsync(Guid id)
        {
            var entityItem = await _dbSet.FindAsync(id.ToString());
            //var entityItem = await _dbSet.FirstOrDefaultAsync(_ => _.Id == id);

            if (!await AnyAsync(id))
            {
                ThrowNotFountException();
            }

            _dbSet.Remove(entityItem);

            await _databaseContext.SaveChangesAsync();
        }

        public async Task RemoveAsync(Expression<Func<T, bool>> filter)
        {
            if (!await AnyAsync(filter))
            {
                return;
            }

            var entityItems = await _dbSet.Where(filter).ToListAsync();

            _dbSet.RemoveRange(entityItems);

            await _databaseContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(T entity)        {

            //if (!await AnyAsync(Guid.Parse(entity.GetType().GetProperty("Id").GetValue(entity).ToString())))
            //{
            //    ThrowNotFountException();
            //}

            _dbSet.Update(entity);
            await _databaseContext.SaveChangesAsync();
        }
        public async Task<IEnumerable<T>> FindListWithCount(int count = 10)
        {
            IQueryable<T> query = _dbSet;
            return await query.Take(count).ToListAsync();
        }

        private void ThrowNotFountException() => throw new ValidationException("С данным ключом значение не найдено!", "");

        private void ThrowConflictException() => throw new ValidationException("С данным ключом значение добавлено ранее!", "");
    }
}
