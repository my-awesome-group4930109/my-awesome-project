﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Developers.OnlineStore.Core.Domain
{
    public partial class Store
    {
        public Store()
        {
            Customers = new HashSet<Customer>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string StoreAdressId { get; set; }

        public virtual StoreAdress StoreAdress { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
    }
}
