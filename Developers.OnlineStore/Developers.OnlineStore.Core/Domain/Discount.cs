﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Developers.OnlineStore.Core.Domain
{
    public partial class Discount
    {
        public Discount()
        {
            Customers = new HashSet<Customer>();
        }

        public string Id { get; set; }
        public double Discount1 { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
    }
}
