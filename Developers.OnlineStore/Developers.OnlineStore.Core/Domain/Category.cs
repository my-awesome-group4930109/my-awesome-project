﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Developers.OnlineStore.Core.Domain
{
    public partial class Category
    {
        public Category()
        {
            Products = new HashSet<Product>();
        }

        public string Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
