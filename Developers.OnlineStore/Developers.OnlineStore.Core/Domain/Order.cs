﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Developers.OnlineStore.Core.Domain
{
    public partial class Order
    {
        public Order()
        {
            OrderStatuses = new HashSet<OrderStatus>();
            ProductOrders = new HashSet<ProductOrder>();
        }

        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string StoreAdressId { get; set; }
        public string DateToDelivery { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual StoreAdress StoreAdress { get; set; }
        public virtual ICollection<OrderStatus> OrderStatuses { get; set; }
        public virtual ICollection<ProductOrder> ProductOrders { get; set; }
    }
}
