﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Developers.OnlineStore.Core.Domain
{
    public partial class ProductPlatform
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual CompatibleProduct CompatibleProduct { get; set; }
    }
}
