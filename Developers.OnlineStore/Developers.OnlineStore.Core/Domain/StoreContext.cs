﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Developers.OnlineStore.Core.Domain
{
    public partial class StoreContext : DbContext
    {
        public StoreContext()
        {
        }

        public StoreContext(DbContextOptions<StoreContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cart> Carts { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<CompatibleProduct> CompatibleProducts { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Discount> Discounts { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderStatus> OrderStatuses { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductCart> ProductCarts { get; set; }
        public virtual DbSet<ProductImage> ProductImages { get; set; }
        public virtual DbSet<ProductOrder> ProductOrders { get; set; }
        public virtual DbSet<ProductPlatform> ProductPlatforms { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<StatusState> StatusStates { get; set; }
        public virtual DbSet<Storage> Storages { get; set; }
        public virtual DbSet<Store> Stores { get; set; }
        public virtual DbSet<StoreAdress> StoreAdresses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlite("DataSource=Store.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cart>(entity =>
            {
                entity.ToTable("Cart");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CustomerId).IsRequired();

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Carts)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("Category");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name).IsRequired();
            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Comment1)
                    .IsRequired()
                    .HasColumnName("Comment");

                entity.Property(e => e.CustomerId).IsRequired();

                entity.Property(e => e.Date).IsRequired();

                entity.Property(e => e.ProductId).IsRequired();

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<CompatibleProduct>(entity =>
            {
                entity.ToTable("CompatibleProduct");

                entity.HasIndex(e => e.PlatformId, "IX_CompatibleProduct_PlatformId")
                    .IsUnique();

                entity.HasIndex(e => e.ProductId, "IX_CompatibleProduct_ProductId")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.PlatformId).IsRequired();

                entity.Property(e => e.ProductId).IsRequired();

                entity.HasOne(d => d.Platform)
                    .WithOne(p => p.CompatibleProduct)
                    .HasForeignKey<CompatibleProduct>(d => d.PlatformId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Product)
                    .WithOne(p => p.CompatibleProduct)
                    .HasForeignKey<CompatibleProduct>(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.ToTable("Customer");

                entity.HasIndex(e => e.Login, "IX_Customer_Login")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DiscountId).IsRequired();

                entity.Property(e => e.Email).IsRequired();

                entity.Property(e => e.FavoriteStoreId).IsRequired();

                entity.Property(e => e.Login).IsRequired();

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.Password).IsRequired();

                entity.Property(e => e.Phone).IsRequired();

                entity.HasOne(d => d.Discount)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.DiscountId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.FavoriteStore)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.FavoriteStoreId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Discount>(entity =>
            {
                entity.ToTable("Discount");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Discount1).HasColumnName("Discount");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.ToTable("Employee");

                entity.HasIndex(e => e.Login, "IX_Employee_Login")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Login).IsRequired();

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.Password).IsRequired();

                entity.Property(e => e.Phone).IsRequired();

                entity.Property(e => e.RoleId).IsRequired();

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Image>(entity =>
            {
                entity.ToTable("Image");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Image1)
                    .IsRequired()
                    .HasColumnName("Image");

                entity.Property(e => e.Name).IsRequired();
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.ToTable("Order");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CustomerId).IsRequired();

                entity.Property(e => e.DateToDelivery).IsRequired();

                entity.Property(e => e.StoreAdressId).IsRequired();

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.CustomerId);

                entity.HasOne(d => d.StoreAdress)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.StoreAdressId);
            });

            modelBuilder.Entity<OrderStatus>(entity =>
            {
                entity.ToTable("OrderStatus");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreationDate).IsRequired();

                entity.Property(e => e.DeliveryDate).IsRequired();

                entity.Property(e => e.OrderId).IsRequired();

                entity.Property(e => e.StatusId).IsRequired();

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrderStatuses)
                    .HasForeignKey(d => d.OrderId);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.OrderStatuses)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("Product");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CategoryId).IsRequired();

                entity.Property(e => e.Description).IsRequired();

                entity.Property(e => e.IsAvailable).HasDefaultValueSql("1");

                entity.Property(e => e.Name).IsRequired();

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ProductCart>(entity =>
            {
                entity.ToTable("ProductCart");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CartId).IsRequired();

                entity.Property(e => e.ProductId).IsRequired();

                entity.HasOne(d => d.Cart)
                    .WithMany(p => p.ProductCarts)
                    .HasForeignKey(d => d.CartId);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductCarts)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ProductImage>(entity =>
            {
                entity.ToTable("ProductImage");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ImageId).IsRequired();

                entity.Property(e => e.ProductId).IsRequired();

                entity.HasOne(d => d.Image)
                    .WithMany(p => p.ProductImages)
                    .HasForeignKey(d => d.ImageId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductImages)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ProductOrder>(entity =>
            {
                entity.ToTable("ProductOrder");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.OrderId).IsRequired();

                entity.Property(e => e.ProductId).IsRequired();

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.ProductOrders)
                    .HasForeignKey(d => d.OrderId);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductOrders)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ProductPlatform>(entity =>
            {
                entity.ToTable("ProductPlatform");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description).IsRequired();

                entity.Property(e => e.Name).IsRequired();
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Role");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description).IsRequired();

                entity.Property(e => e.Name).IsRequired();
            });

            modelBuilder.Entity<StatusState>(entity =>
            {
                entity.ToTable("StatusState");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name).IsRequired();
            });

            modelBuilder.Entity<Storage>(entity =>
            {
                entity.ToTable("Storage");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ProductId).IsRequired();

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Storages)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Store>(entity =>
            {
                entity.ToTable("Store");

                entity.HasIndex(e => e.StoreAdressId, "IX_Store_StoreAdressId")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.StoreAdressId).IsRequired();

                entity.HasOne(d => d.StoreAdress)
                    .WithOne(p => p.Store)
                    .HasForeignKey<Store>(d => d.StoreAdressId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<StoreAdress>(entity =>
            {
                entity.ToTable("StoreAdress");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Area).IsRequired();

                entity.Property(e => e.Building).IsRequired();

                entity.Property(e => e.City).IsRequired();

                entity.Property(e => e.DaysForDelivery).HasDefaultValueSql("1");

                entity.Property(e => e.Street).IsRequired();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
