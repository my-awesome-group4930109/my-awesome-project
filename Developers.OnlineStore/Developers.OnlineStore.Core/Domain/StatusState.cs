﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Developers.OnlineStore.Core.Domain
{
    public partial class StatusState
    {
        public StatusState()
        {
            OrderStatuses = new HashSet<OrderStatus>();
        }

        public string Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<OrderStatus> OrderStatuses { get; set; }
    }
}
