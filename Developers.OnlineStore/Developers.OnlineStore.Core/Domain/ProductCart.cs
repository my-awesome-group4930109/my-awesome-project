﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Developers.OnlineStore.Core.Domain
{
    public partial class ProductCart
    {
        public string Id { get; set; }
        public string ProductId { get; set; }
        public string CartId { get; set; }
        public long Count { get; set; }

        public virtual Cart Cart { get; set; }
        public virtual Product Product { get; set; }
    }
}
