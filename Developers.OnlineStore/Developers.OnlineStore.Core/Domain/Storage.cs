﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Developers.OnlineStore.Core.Domain
{
    public partial class Storage
    {
        public string Id { get; set; }
        public string ProductId { get; set; }
        public long Count { get; set; }

        public virtual Product Product { get; set; }
    }
}
