﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Developers.OnlineStore.Core.Domain
{
    public partial class Employee
    {
        public string Id { get; set; }
        public string RoleId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public virtual Role Role { get; set; }
    }
}
