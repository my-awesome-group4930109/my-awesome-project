﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Developers.OnlineStore.Core.Domain
{
    public partial class OrderStatus
    {
        public string Id { get; set; }
        public string StatusId { get; set; }
        public string OrderId { get; set; }
        public string CreationDate { get; set; }
        public string DeliveryDate { get; set; }

        public virtual Order Order { get; set; }
        public virtual StatusState Status { get; set; }
    }
}
