﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Developers.OnlineStore.Core.Domain
{
    public partial class CompatibleProduct
    {
        public string Id { get; set; }
        public string ProductId { get; set; }
        public string PlatformId { get; set; }

        public virtual ProductPlatform Platform { get; set; }
        public virtual Product Product { get; set; }
    }
}
