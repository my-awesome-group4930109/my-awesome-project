﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Developers.OnlineStore.Core.Domain
{
    public partial class StoreAdress
    {
        public StoreAdress()
        {
            Orders = new HashSet<Order>();
        }

        public string Id { get; set; }
        public string Area { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public long DaysForDelivery { get; set; }

        public virtual Store Store { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
