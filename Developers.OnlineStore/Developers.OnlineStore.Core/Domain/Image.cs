﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Developers.OnlineStore.Core.Domain
{
    public partial class Image
    {
        public Image()
        {
            ProductImages = new HashSet<ProductImage>();
        }

        public string Id { get; set; }
        public string Image1 { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ProductImage> ProductImages { get; set; }
    }
}
