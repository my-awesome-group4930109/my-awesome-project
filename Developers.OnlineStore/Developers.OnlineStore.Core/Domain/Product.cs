﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Developers.OnlineStore.Core.Domain
{
    public partial class Product
    {
        public Product()
        {
            Comments = new HashSet<Comment>();
            ProductCarts = new HashSet<ProductCart>();
            ProductImages = new HashSet<ProductImage>();
            ProductOrders = new HashSet<ProductOrder>();
            Storages = new HashSet<Storage>();
        }

        public string Id { get; set; }
        public string CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public long IsAvailable { get; set; }

        public virtual Category Category { get; set; }
        public virtual CompatibleProduct CompatibleProduct { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<ProductCart> ProductCarts { get; set; }
        public virtual ICollection<ProductImage> ProductImages { get; set; }
        public virtual ICollection<ProductOrder> ProductOrders { get; set; }
        public virtual ICollection<Storage> Storages { get; set; }
    }
}
