﻿using Developers.ProductService.Models;

namespace Developers.ProductService.ResponseModel
{
    public class CategoryProduct
    {
        public string CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public string ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}
