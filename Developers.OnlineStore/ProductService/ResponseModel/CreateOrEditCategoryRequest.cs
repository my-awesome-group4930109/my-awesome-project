﻿namespace Developers.ProductService.ResponseModel
{
    public class CreateOrEditCategoryRequest
    {
        public string Name { get; set; }
        public List<string> ProductIds { get; set; }
    }
}
