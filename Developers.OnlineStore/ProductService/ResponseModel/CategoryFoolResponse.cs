﻿using Developers.ProductService.Models;
using System.Reflection.PortableExecutable;

namespace Developers.ProductService.ResponseModel
{
    public class CategoryFoolResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<ProductResponse> Products { get; set; }
        public CategoryFoolResponse() { }
        public CategoryFoolResponse(Category category)
        {
            Id = category.Id;
            Name = category.Name;
            Products = category.Products.Select(x => new ProductResponse()
            {
                Id = x.ProductId,
                Name = x.Product.Name,
                ArticleNumber = x.Product.ArticleNumber,
                CategoryId = x.Product.CategoryId,
                Description = x.Product.Description,
                Characteristic = x.Product.Characteristic,
                ImagePath = x.Product.ImagePath,
                Price = x.Product.Price,
                Count = x.Product.Count,
                IsAvailable = x.Product.IsAvailable
            }).ToList();
        }
    }
}
