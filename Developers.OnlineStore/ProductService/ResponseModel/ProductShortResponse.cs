﻿using System.ComponentModel.DataAnnotations;

namespace Developers.ProductService.ResponseModel
{
    public class ProductShortResponse
    {
        public string Id { get; set; }
        public string? ArticleNumber { get; set; }
        //Название товара
        [Required(ErrorMessage = "Обязательное поле")]
        public string Name { get; set; }
        public string? ImagePath { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        public int Price { get; set; }
        //текущее количество на складе
        public int? Count { get; set; }
        //Доступность к продаже
        public long IsAvailable { get; set; }
    }
}
