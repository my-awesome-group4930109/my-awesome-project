﻿namespace Developers.ProductService.ResponseModel
{
    public class CategoryResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }        
    }
}
