﻿using Developers.ProductService.Models;
using Developers.ProductService.ResponseModel;

namespace Developers.ProductService.Mappers
{
    public class CategoryMapper
    {
        public static Category MapFromModel(CreateOrEditCategoryRequest model, IEnumerable<Product> products, Category category = null)
        {
            if (category == null)
            {
                category = new Category();
                category.Id = Guid.NewGuid().ToString();
            }

            category.Name = model.Name;
            category.Products = products.Select(x => new CategoryProduct()
            {
                CategoryId = category.Id,
                Product = x,
                ProductId = x.Id
            }).ToList();
            return category;
        }

        public static Category MapFromModel(Category category, IEnumerable<Product> products)
        {
            if (category == null)
            {
                category = new Category();
                category.Id = Guid.NewGuid().ToString();
            }

            category.Name = category.Name;

            category.Products = products.Select(x => new CategoryProduct()
            {
                CategoryId = category.Id,
                Product = x,
                ProductId = x.Id
            }).ToList();

            return category;
        }
        public static Category MapFromModel(CategoryResponse category)
        {
            Category data = new Category();
            if (String.IsNullOrEmpty(category.Id))
            {
                data.Id = Guid.NewGuid().ToString();
            }
            else data.Id = category.Id;
            data.Name = category.Name;

            return data;
        }

    }
}
