﻿namespace Developers.ProductService.Models
{
    public class ProductCount
    {
        public string ProductId { get; set; }
        public int Count { get; set; }
        
    }
}
