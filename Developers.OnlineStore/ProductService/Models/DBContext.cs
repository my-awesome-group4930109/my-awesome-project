﻿using Developers.ProductService.ResponseModel;
using Microsoft.EntityFrameworkCore;

namespace Developers.ProductService.Models
{

    public partial class DBContext : DbContext
    {
        public DBContext(DbContextOptions<DBContext> options)
            : base(options)
        {
        }
        public DbSet<Category> Category { get; set; }
        public DbSet<Product> Product { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite("DataSource=Product.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CategoryProduct>()
                .HasKey(bc => new { bc.CategoryId , bc.ProductId});
            modelBuilder.Entity<CategoryProduct>()
                .HasOne(bc => bc.Category)
                .WithMany(b => b.Products)
                .HasForeignKey(bc => bc.CategoryId);
            modelBuilder.Entity<CategoryProduct>()
                .HasOne(bc => bc.Product)
                .WithMany()
                .HasForeignKey(bc => bc.ProductId);
            
            //modelBuilder.Entity<Category>(entity =>
            //{
            //    entity.ToTable("Category");

            //    entity.Property(e => e.Id).HasColumnName("id");

            //    entity.Property(e => e.Name).IsRequired();
            //    entity.Property(e => e.Name).HasMaxLength(250);
            //});            //modelBuilder.Entity<Product>(entity =>
            //{
            //    entity.ToTable("Product");

            //    entity.Property(e => e.Id).HasColumnName("id");

            //    entity.Property(e => e.CategoryId).IsRequired();

            //    entity.Property(e => e.Description).IsRequired();

            //    entity.Property(e => e.IsAvailable).HasDefaultValueSql("1");

            //    entity.Property(e => e.Name).IsRequired();

            //    entity.HasOne(d => d.Category)
            //        .WithMany(p => p.Products)
            //        .HasForeignKey(d => d.CategoryId)
            //        .OnDelete(DeleteBehavior.ClientSetNull);
            //});

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
