﻿using Developers.ProductService.ResponseModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Developers.ProductService.Models
{
    public class Category : BaseEntity
    {
        [Required(ErrorMessage = "Обязательное поле")]
        public string Name { get; set; }
        public virtual ICollection<CategoryProduct>? Products { get; set; }
    }
}
