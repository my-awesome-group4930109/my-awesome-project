﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Developers.ProductService.Models
{
    public class Product : BaseEntity
    {
        //Артикул
        public string? ArticleNumber { get; set; }
        //Название товара
        [Required(ErrorMessage = "Обязательное поле")]
        public string Name { get; set; }
        //Категория
        [Required(ErrorMessage = "Обязательное поле")] 
        public string CategoryId { get; set; }
        //Описание товара
        public string? Description { get; set; }
        //Характеристика товара
        public string? Characteristic { get; set; }
        //Картинка изображения
        public string? ImagePath { get; set; }
        //Цена
        [Required(ErrorMessage = "Обязательное поле")] 
        public int Price { get; set; }
        //текущее количество на складе
        public int? Count { get; set; }
        //Доступность к продаже
        public long IsAvailable { get; set; }

        public Category Category { get; set; }

    }
}
