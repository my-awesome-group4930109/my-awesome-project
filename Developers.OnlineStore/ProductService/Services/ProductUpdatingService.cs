﻿using Developers.ProductService.IRepositories;
using Developers.ProductService.Models;
using Microsoft.AspNetCore.Mvc;

namespace Developers.ProductService.Services
{
    public class ProductUpdatingService: IProductUpdatingService
    {
        private readonly IRepository<Product> _product;

        public ProductUpdatingService(IRepository<Product> product)
        {
            _product = product;
        }
        public async Task UpdateProduct(ProductCount nProductCount)
        {
            var products = _product.GetByIdAsync(nProductCount.ProductId).Result;
            if (products == default)
                return;
            if (products.Count < nProductCount.Count) return ;
            var res = products.Count - nProductCount.Count;
            products.IsAvailable = (res >= 0 ? products.IsAvailable : 0);
            products.Count = res;

            await _product.EditAsync(products);
        }
    }
}
