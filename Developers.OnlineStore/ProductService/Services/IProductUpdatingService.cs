﻿using Developers.ProductService.Models;

namespace Developers.ProductService.Services
{
    public interface IProductUpdatingService
    {
        public Task UpdateProduct(ProductCount nCount);
    }
}
