﻿namespace Developers.ProductService.Messages
{
    public class MessageDto
    {
        public string Content { get; set; }
    }
}
