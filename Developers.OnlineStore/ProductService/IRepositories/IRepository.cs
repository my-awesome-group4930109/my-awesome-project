﻿using System.Linq.Expressions;

namespace Developers.ProductService.IRepositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(string id);
        Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate);
        Task AddAsync(T entity);
        Task EditAsync(T entity);
        Task DeleteAsync(string id);
    }
}
