﻿using Developers.ProductService.Messages;
using Developers.ProductService.Models;
using Developers.ProductService.Services;
using MassTransit;
using System.Text.Json;

namespace Developers.ProductService.Consumers
{
    public class ProductConsumer : IConsumer
    {
        private readonly IProductUpdatingService _productService;
        public ProductConsumer(IProductUpdatingService productService)
        {
            _productService = productService;
        }
        public async Task Consume(ConsumeContext<MessageDto> context)
        {
            if (context.Message.Content != null)
            {
                var response = JsonSerializer.Deserialize<ProductCount>(context.Message.Content);
                await _productService.UpdateProduct(response);
            }
        }
    }
}