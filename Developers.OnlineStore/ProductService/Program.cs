using Developers.ProductService.IRepositories;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Developers.ProductService.Repositories;
using Microsoft.Extensions.Configuration;
using System;
using Developers.ProductService.Models;
using MassTransit;
using Developers.ProductService.Consumers;
using Developers.ProductService.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
var connectionString = builder.Configuration.GetConnectionString("SQlConnection");
builder.Services
    .AddDbContext<DBContext>(options => options.UseSqlite(connectionString));
builder.Services.AddScoped(typeof(IRepository<Category>), typeof(EfRepository<Category>));
builder.Services.AddScoped(typeof(IRepository<Product>), typeof(EfRepository<Product>));
#region MassTransit
//builder.Services.AddTransient<IProductService, SliceService>();
builder.Services.AddTransient<IProductUpdatingService, ProductUpdatingService>();
builder.Services.AddMassTransit(c =>
{
    c.AddConsumer<ProductConsumer>();
    c.UsingRabbitMq((context, cfg) =>
    {
        cfg.UseMessageRetry(r => 
        {
            r.Exponential(5,
                       TimeSpan.FromSeconds(1),
                       TimeSpan.FromSeconds(100),
                       TimeSpan.FromSeconds(5));
            r.Ignore <StackOverflowException>();
            r.Ignore <ArgumentNullException>(x => x.Message.Contains("Consumer"));
            });

        cfg.Host(builder.Configuration["RABBITMQ_HOST"] ?? "localhost", "/", h =>
            {
                h.Username("guest");
                h.Password("guest");
                h.Heartbeat(TimeSpan.FromMinutes(1));
            });
        cfg.ConfigureEndpoints(context);
        cfg.UseRawJsonSerializer();
    });
});
#endregion

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseRouting();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});
app.UseAuthorization();

app.MapControllers();

app.Run();
