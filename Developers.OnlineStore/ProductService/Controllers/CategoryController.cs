﻿using Developers.ProductService.IRepositories;
using Developers.ProductService.Mappers;
using Developers.ProductService.Models;
using Developers.ProductService.ResponseModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductService.Controllers;

namespace Developers.ProductService.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    
    public class CategoryController : ControllerBase
    {
        private readonly ILogger<CategoryController> _logger;
        private readonly IRepository<Category> _category;
        private readonly IRepository<Product> _product;
        public CategoryController(IRepository<Category> category, IRepository<Product> product, ILogger<CategoryController> logger)//
        {
            _logger = logger;
            _category = category;
            _product = product;
        }
        [HttpGet]
        public async Task<ActionResult<List<CategoryResponse>>> GetCategorysAsync()
        {
            var categorys = await _category.GetAllAsync();
            if (categorys == null)
                return NotFound("Категории товара не найдены");
            var response = categorys.Select(x => new CategoryResponse()
            {
                Id = x.Id,
                Name = x.Name
            });

            return Ok(response);
        }

        /// <summary>
        /// Создать новую категорию (без продукции)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CategoryResponse>> CreateCategoryAsync(CategoryResponse request)
        {
            //Category category = CategoryMapper.MapFromModel(request);
            Category category = new Category();
            if (String.IsNullOrEmpty(request.Id))
            {
                category.Id = Guid.NewGuid().ToString();
            }
            else category.Id = request.Id;
            category.Name = request.Name;
            await _category.AddAsync(category);
            return Ok();
        }

        /// <summary>
        /// Получить информацию категории товара
        /// </summary>
        /// <param name="id">Id категории товара</example></param>
        [HttpGet("{id}")]
        public async Task<ActionResult<List<Category>>> GetCategoryAsync(string id)
        {
            var product = await _product.GetWhere(x => x.CategoryId.Equals(id));
            var categorysDb = await _category.GetByIdAsync(id);
            if (categorysDb == null) return NotFound();
            var category = CategoryMapper.MapFromModel(categorysDb, product);
            var response = new CategoryFoolResponse(category);

            return Ok(response);
        }

        /// <summary>
        /// Удалить категорию товара
        /// </summary>
        /// <param name="id">Id категории, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategoryAsync(string id)
        {
            var category = await _category.GetByIdAsync(id);

            if (category == null)
                return NotFound();
            if (category.Products != null) return BadRequest("Данная категория содержит товары");
            await _category.DeleteAsync(id);
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditCategoryAsync(string id, CreateOrEditCategoryRequest request)
        {
            var category = await _category.GetByIdAsync(id);

            if (category == null)
                return NotFound();

            var products = await _product.GetWhere(x => x.CategoryId.Equals(id));

            CategoryMapper.MapFromModel(request, products, category);
            await _category.EditAsync(category);

            return Ok();
        }
    }
}
