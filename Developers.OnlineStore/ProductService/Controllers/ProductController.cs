﻿using Developers.ProductService.IRepositories;
using Developers.ProductService.Mappers;
using Developers.ProductService.Models;
using Developers.ProductService.ResponseModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Reflection.PortableExecutable;

namespace Developers.ProductService.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly ILogger<CategoryController> _logger;
        private readonly IRepository<Category> _category;
        private readonly IRepository<Product> _product;
        public ProductController(IRepository<Category> category, IRepository<Product> product, ILogger<CategoryController> logger)
        {
            _logger = logger;
            _category = category;
            _product = product;
        }
        [HttpGet]
        //[Route("GetProductAll")]
        public async Task<ActionResult<List<ProductResponse>>> GetProductsAsync()
        {
            var products = await _product.GetAllAsync();
            var category = _category.GetAllAsync();
            if (products == null)
                return NotFound("Товары не найдены");
            var response = products.Select(x => new ProductShortResponse()
            {
                Id = x.Id,
                Name = x.Name,
                ArticleNumber = x.ArticleNumber,
                ImagePath = x.ImagePath,
                Price = x.Price,
                Count = x.Count,
                IsAvailable = x.IsAvailable
            });
            return Ok(response);
        }

        /// <summary>
        /// Получить продукты
        /// </summary>
        /// <param name="id">Id товара</example></param>
        [HttpGet("{id}")]
        //[Route("GetProductById")]
        public async Task<ActionResult<List<ProductResponse>>> GetProductAsync(string id)
        {
            List<string> nList = id.Split(",").ToList();
            var product = await _product.GetWhere(x => nList.Contains(x.Id));
            if (product == null) return NotFound();
            return Ok(product);
        }

        /// <summary>
        /// Удалить категорию товара
        /// </summary>
        /// <param name="id">Id категории, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProductAsync(string id)
        {
            var product = await _product.GetByIdAsync(id);

            if (product == null)
                return NotFound();
            await _product.DeleteAsync(id);
            return Ok();
        }
        [HttpPost]
        public async Task<ActionResult<ProductResponse>> CreateProductAsync(ProductResponse request)
        {
            Product product = new Product();
            if (String.IsNullOrEmpty(request.Id))
            {
                product.Id = Guid.NewGuid().ToString();
            }
            else product.Id = request.Id;
            product.Name = request.Name;
            product.ArticleNumber = request.ArticleNumber;
            product.CategoryId = request.CategoryId;
            product.Description = request.Description;
            product.Characteristic = request.Characteristic;
            product.ImagePath = request.ImagePath;
            product.Price = request.Price;
            product.Count = request.Count;
            product.IsAvailable = request.IsAvailable;

            await _product.AddAsync(product);
            return Ok();
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> EditProductAsync(string id, ProductResponse request)
        {
            var product = await _product.GetByIdAsync(id);

            if (product == null)
                return NotFound();

            product.Name = request.Name;
            product.ArticleNumber = request.ArticleNumber;
            product.ImagePath = request.ImagePath;
            product.Price = request.Price;
            product.CategoryId = request.CategoryId;
            product.Description = request.Description;
            product.Characteristic = request.Characteristic;
            product.Count = request.Count;
            product.IsAvailable = request.IsAvailable;
            await _product.EditAsync(product);

            return Ok();
        }
        /// <summary>
        /// Изменение количества товара на складе
        /// </summary>ленного товара
        /// <param name="id"></param>
        /// <param name="count">Количество купленного товара</param>
        /// <returns></returns>
        [HttpPut]
        //[Route("EditProductCount")]
        public async Task<IActionResult> EditProductCountAsync(string id, int count)
        {
            var product = await _product.GetByIdAsync(id);

            if (product == null)
                return NotFound();
            if (product.Count < count) return BadRequest("Недостаточно товара на складе");
            
            product.Count = product.Count - count;
            await _product.EditAsync(product);

            return Ok();
        }
    }
}
